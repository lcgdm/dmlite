/*
 * Copyright (C) 2015 by CERN/IT/SDC/ID. All rights reserved.
 *
 * File:   XrdDPMStatInfo.hh
 * Author: David Smith
 *
 */

#ifndef XRDDPMSTATINFO_HH
#define XRDDPMSTATINFO_HH

#include <XrdVersion.hh>
#include <vector>
#include <string>

// to allow building with xrootd releases before 4.3
#if XrdVNUMBER<40300
class XrdOucName2NameVec
{
public:
   virtual std::vector<std::string *> *n2nVec(const char *lfn)=0;
   virtual void Recycle(std::vector<std::string *> *nvP) {
      if (nvP) {
         for (unsigned int i = 0; i < nvP->size(); i++) {
            delete (*nvP)[i];
         }
         delete nvP;
      }
   }
   XrdOucName2NameVec() {}
   virtual ~XrdOucName2NameVec() {}
};
#endif

#endif  /* XRDDPMSTATINFO_HH */
