cmake_minimum_required (VERSION 2.6)



IF(OVERWRITE_CONFIGFILES)
  configure_file (xrootd/xrootd-dpmdisk.cfg
                ${CMAKE_CURRENT_BINARY_DIR}/xrootd/xrootd-dpmdisk.cfg
                @ONLY)
  configure_file (xrootd/xrootd-dpmfedredir_atlas.cfg
                ${CMAKE_CURRENT_BINARY_DIR}/xrootd/xrootd-dpmfedredir_atlas.cfg
                @ONLY)
  configure_file (xrootd/xrootd-dpmredir.cfg
                ${CMAKE_CURRENT_BINARY_DIR}/xrootd/xrootd-dpmredir.cfg
                @ONLY)
  install (FILES     ${CMAKE_CURRENT_BINARY_DIR}/xrootd/xrootd-dpmdisk.cfg
                     ${CMAKE_CURRENT_BINARY_DIR}/xrootd/xrootd-dpmfedredir_atlas.cfg
                     ${CMAKE_CURRENT_BINARY_DIR}/xrootd/xrootd-dpmredir.cfg
                     DESTINATION ${INSTALL_PFX_ETC}/xrootd)
ELSE()
  configure_file (xrootd/xrootd-dpmdisk.cfg
                ${CMAKE_CURRENT_BINARY_DIR}/xrootd/xrootd-dpmdisk.cfg.example
                @ONLY)
  configure_file (xrootd/xrootd-dpmfedredir_atlas.cfg
                ${CMAKE_CURRENT_BINARY_DIR}/xrootd/xrootd-dpmfedredir_atlas.cfg.example
                @ONLY)
  configure_file (xrootd/xrootd-dpmredir.cfg
                ${CMAKE_CURRENT_BINARY_DIR}/xrootd/xrootd-dpmredir.cfg.example
                @ONLY)
  install (FILES     ${CMAKE_CURRENT_BINARY_DIR}/xrootd/xrootd-dpmdisk.cfg.example
                     ${CMAKE_CURRENT_BINARY_DIR}/xrootd/xrootd-dpmfedredir_atlas.cfg.example
                     ${CMAKE_CURRENT_BINARY_DIR}/xrootd/xrootd-dpmredir.cfg.example
                     DESTINATION ${INSTALL_PFX_ETC}/xrootd)
ENDIF()
