/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE
#include <apr_strings.h>
#include <apr_xml.h>
#include <ctype.h>
#include <dmlite/c/catalog.h>
#include <httpd.h>
#include <http_log.h>
#include <inttypes.h>

#include "acl.h"
#include "mime.h"
#include "mod_lcgdm_ns.h"
#include "replicas.h"
#include "../shared/utils.h"

/** Not defined in mod_dav.h. */
enum
{
    /* Non-standard */
    DAV_PROPID_executable,
    DAV_PROPID_executable_apache,
    DAV_PROPID_iscollection,
    /* LCGDM specific */
    DAV_PROPID_guid,
    DAV_PROPID_mode,
    DAV_PROPID_sumtype,
    DAV_PROPID_sumvalue,
    DAV_PROPID_replicas,
    DAV_PROPID_fileid,
    DAV_PROPID_status,
    DAV_PROPID_lastaccess,
    DAV_PROPID_xattr,
    /* Defined by http://webdav.org/specs/rfc3744.html (ACL)*/
    DAV_PROPID_owner,
    DAV_PROPID_group,
    DAV_PROPID_supported_privilege_set,
    DAV_PROPID_acl,
    /* Defined by https://tools.ietf.org/html/rfc4331 (quotas) */
    DAV_PROPID_quota_used,
    DAV_PROPID_quota_free
};

/** Namespace definitions. */
static const char * const dav_ns_namespace_uris[] = {
        "DAV:", "http://apache.org/dav/props/", "LCGDM:", NULL };
enum
{
    DAV_URI_DAV, DAV_URI_APACHE, DAV_URI_LCGDM
};

/** Used for the property context. */
typedef union
{
    char flag;
    uid_t uid;
    gid_t gid;
    mode_t mode;
    const char *string;
    time_t tim;
    struct dav_ns_replica_array replica_array;
    dmlite_any_dict* dict;
} dav_ns_liveprop_context;

/** Properties definition */
static const dav_liveprop_spec dav_ns_props[] =
{
    /* Standard DAV properties */
    {
        DAV_URI_DAV, "creationdate",
        DAV_PROPID_creationdate, 0
    },
    {
        DAV_URI_DAV, "getlastmodified",
        DAV_PROPID_getlastmodified, 1 },
    {
        DAV_URI_LCGDM, "lastaccessed",
        DAV_PROPID_lastaccess, 1
    },
    {
        DAV_URI_DAV, "getetag",
        DAV_PROPID_getetag, 0
    },
    {
        DAV_URI_DAV, "getcontentlength",
        DAV_PROPID_getcontentlength, 0
    },
    {
        DAV_URI_DAV, "displayname",
        DAV_PROPID_displayname, 0
    },
    {
        DAV_URI_DAV, "getcontenttype",
        DAV_PROPID_getcontenttype, 0
    },
    /* Standard from DeltaV */
    {
        DAV_URI_DAV, "comment",
        DAV_PROPID_comment, 1
    },
    /* Non standard */
    {
        DAV_URI_DAV, "executable",
        DAV_PROPID_executable, 1
    },
    {
        DAV_URI_APACHE, "executable",
        DAV_PROPID_executable_apache, 1
    },
    {
        DAV_URI_DAV, "iscollection",
        DAV_PROPID_iscollection, 0
    },
    /* LCGDM specific */
    {
        DAV_URI_LCGDM, "guid",
        DAV_PROPID_guid, 1
    },
    {
        DAV_URI_LCGDM, "mode",
        DAV_PROPID_mode, 1
    },
    {
        DAV_URI_LCGDM, "sumtype",
        DAV_PROPID_sumtype, 0
    },
    {
        DAV_URI_LCGDM, "sumvalue",
        DAV_PROPID_sumvalue, 0
    },
    {
        DAV_URI_LCGDM, "replicas",
        DAV_PROPID_replicas, 1
    },
    {
        DAV_URI_LCGDM, "fileid",
        DAV_PROPID_fileid, 0
    },
    {
        DAV_URI_LCGDM, "status",
        DAV_PROPID_status, 0
    },
    {
        DAV_URI_LCGDM, "xattr",
        DAV_PROPID_xattr, 1
    },
    /* ACL RFC  */
    {
        DAV_URI_DAV, "owner",
        DAV_PROPID_owner, 1
    },
    {
        DAV_URI_DAV, "group",
        DAV_PROPID_group, 1
    },
    {
        DAV_URI_DAV, "supported-privilege-set",
        DAV_PROPID_supported_privilege_set, 0
    },
    {
        DAV_URI_DAV, "acl",
        DAV_PROPID_acl, 0
    },
    /* RFC 4331 (quotas) */
    {
        DAV_URI_DAV, "quota-used-bytes",
        DAV_PROPID_quota_used, 0
    },
    {
        DAV_URI_DAV, "quota-available-bytes",
        DAV_PROPID_quota_free, 0
    },
    /* END */
    {0, NULL, 0, 0 }
};

/**
 * Insert a property into the output (retrieve property).
 * @param resource The resource being queried.
 * @param propid   The property ID.
 * @param what     What to get.
 * @param phdr     Where to put.
 * @return         Operation status.
 */
static dav_prop_insert dav_ns_insert_prop(const dav_resource *resource,
        int propid, dav_prop_insert what, apr_text_header *phdr)
{
    int64_t free_space, used_space;
    char *s;
    char buffer[PATH_MAX];
    char *value = buffer;
    unsigned global_ns, nreplies;
    const dav_liveprop_spec *prop_info;
    dmlite_replica *replicas;
    dmlite_context *ctx = resource->info->ctx;

    /* No existence, no properties */
    if (!resource->exists)
        return DAV_PROP_INSERT_NOTDEF;

    /* Gimme! */
    switch (propid) {

        /* Creation date, RFC3339 expected */
        case DAV_PROPID_creationdate:
            dav_shared_format_datetime(buffer, sizeof(buffer),
                    resource->info->stat.stat.st_ctime, DAV_DPM_RFC3339);
            break;

            /* Last modification date, RFC1123 expected */
        case DAV_PROPID_getlastmodified:
            dav_shared_format_datetime(buffer, sizeof(buffer),
                    resource->info->stat.stat.st_mtime, DAV_DPM_RFC2068);
            break;

            /* Last access time */
        case DAV_PROPID_lastaccess:
            dav_shared_format_datetime(buffer, sizeof(buffer),
                    resource->info->stat.stat.st_atime, DAV_DPM_RFC2068);
            break;

            /* Size of the file. Number of entries for directories */
        case DAV_PROPID_getcontentlength:
            if (resource->collection)
                return DAV_PROP_INSERT_NOTDEF;
            else
                snprintf(buffer, sizeof(buffer), "%zd",
                        (size_t) resource->info->stat.stat.st_size);
            break;

            /* 1 if the resource is a collection. Used by Windows Explorer */
        case DAV_PROPID_iscollection:
            value[0] = resource->collection ? '1' : '0';
            value[1] = '\0';
            break;

            /* Return 'T' if the file is executable */
        case DAV_PROPID_executable:
        case DAV_PROPID_executable_apache:
            if (resource->collection) {
                return DAV_PROP_INSERT_NOTDEF;
            }
            else {
                value[0] =
                        (resource->info->stat.stat.st_mode & S_IXUSR) ?
                                'T' : 'F';
                value[1] = '\0';
            }
            break;

            /* Name used for displaying */
        case DAV_PROPID_displayname:
            value = resource->info->stat.name;
            break;

            /* E-Tag, used for caching */
        case DAV_PROPID_getetag:
            value = (char*) resource->hooks->getetag(resource);
            break;

            /* Content-type (MIME) */
        case DAV_PROPID_getcontenttype:
            if (resource->collection) {
                return DAV_PROP_INSERT_NOTDEF;
            }
            else {
                /* Try with the content-type xattr */
                dmlite_any* any = dmlite_any_dict_get(
                        resource->info->stat.extra, "content-type");
                if (any != NULL ) {
                    dmlite_any_to_string(any, buffer, sizeof(buffer));
                    dmlite_any_free(any);
                }
                /* Try with the mime file */
                else if (dav_ns_mime_get(buffer, sizeof(buffer),
                        resource->info->stat.name) == NULL )
                    return DAV_PROP_INSERT_NOTDEF;
            }
            break;

            /* GUID */
        case DAV_PROPID_guid:
            value = resource->info->stat.guid;
            break;

            /* Mode bits */
        case DAV_PROPID_mode:
            snprintf(buffer, sizeof(buffer), "0%o",
                    resource->info->stat.stat.st_mode);
            break;

            /* Checksum type */
        case DAV_PROPID_sumtype:
            value = resource->info->stat.csumtype;
            break;

            /* Checksum value */
        case DAV_PROPID_sumvalue:
            value = resource->info->stat.csumvalue;
            break;

            /* The owner user */
        case DAV_PROPID_owner:
            snprintf(buffer, sizeof(buffer), "%u",
                    resource->info->stat.stat.st_uid);
            break;

            /* The group. Name, or GID if not found */
        case DAV_PROPID_group:
            snprintf(buffer, sizeof(buffer), "%u",
                    resource->info->stat.stat.st_gid);
            break;

            /* Supported privilege set */
        case DAV_PROPID_supported_privilege_set:
            value = (char*) dav_ns_supported_privilege_set();
            break;

            /* ACL */
        case DAV_PROPID_acl:
            value = dav_ns_acl_format(resource->info->request,
                    resource->info->stat.acl);
            break;

            /* Quota, used bytes */
        case DAV_PROPID_quota_used:
            if (!resource->collection)
                return DAV_PROP_INSERT_NOTDEF;
            snprintf(buffer, sizeof(buffer), "%zd",
                    (size_t) resource->info->stat.stat.st_size);;
            break;

            /* Quota, free bytes */
        case DAV_PROPID_quota_free:
            if (!resource->collection)
                return DAV_PROP_INSERT_NOTDEF;
            if (dmlite_getdirspaces(ctx, resource->info->sfn, &free_space, &used_space) < 0) {
                return DAV_PROP_INSERT_NOTDEF;
            }
            snprintf(buffer, sizeof(buffer), "%"PRId64, free_space);
            break;

            /* Associated comment */
        case DAV_PROPID_comment:
            value[0] = '\0';
            dmlite_getcomment(ctx, resource->info->sfn, value, sizeof(buffer));
            break;

            /* List of replicas */
        case DAV_PROPID_replicas:
            if (resource->collection)
                return DAV_PROP_INSERT_NOTDEF;
            else if (dmlite_getreplicas(ctx, resource->info->sfn, &nreplies,
                    &replicas) == 0 && nreplies) {
                value = dav_ns_serialize_replicas(resource->info->request,
                        nreplies, replicas);
                dmlite_replicas_free(nreplies, replicas);
            }
            else {
                value[0] = '\0';
            }
            break;

            /* File ID */
        case DAV_PROPID_fileid:
            snprintf(value, sizeof(buffer), "%lu",
                    resource->info->stat.stat.st_ino);
            break;

            /* Status */
        case DAV_PROPID_status:
            value[0] = resource->info->stat.status;
            value[1] = '\0';
            break;

            /* Extended attributes */
        case DAV_PROPID_xattr:
            if (resource->info->stat.extra)
                dmlite_any_dict_to_json(resource->info->stat.extra, value,
                        sizeof(buffer));
            else
                value[0] = '\0';
            break;

        default:
            /* Why are we here? Where did that come from? */
            ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, resource->info->request,
                    "Something weird happened here: propid=%d", propid);
            return DAV_PROP_INSERT_NOTDEF;
    }

    /* Insert into the output */
    global_ns = dav_get_liveprop_info(propid, &dav_ns_liveprop_group,
            &prop_info);

    /* Asking for the value? */
    if (what == DAV_PROP_INSERT_VALUE) {
        s = apr_psprintf(resource->pool, "<lp%d:%s>%s</lp%d:%s>", global_ns,
                prop_info->name, apr_xml_quote_string(resource->pool, value, 0),
                global_ns, prop_info->name);
    }
    /* Asking for the name? */
    else if (what == DAV_PROP_INSERT_NAME) {
        s = apr_psprintf(resource->pool, "<lp%d:%s/>", global_ns,
                prop_info->name);
    }
    /* Or just asking if it supported? */
    else {
        /* DAV_PROP_INSERT_SUPPORTED */
        s = apr_psprintf(resource->pool,
                "<D:supported-live-property D:name=\"%s\" "
                        "D:namespace=\"%s\"/>", prop_info->name,
                dav_ns_namespace_uris[prop_info->ns]);
    }
    apr_text_append(resource->pool, phdr, s);

    /* Done */
    return what;
}

/**
 * Is the property writable?
 * @param resource The resource being queried.
 * @param propid   The property ID.
 * @return         1 if it is, 0 if not.
 */
static int dav_ns_is_writable(const dav_resource *resource, int propid)
{
    (void) resource;

    const dav_liveprop_spec *prop_info;

    dav_get_liveprop_info(propid, &dav_ns_liveprop_group, &prop_info);
    return prop_info->is_writable;
}

/**
 * Check if the passed value for the given property is good enough.
 * @param resource  The resource being modified.
 * @param elem      The value for the property.
 * @param operation Set or delete.
 * @param context   Can be used to pass values to the rest of the functions.
 * @param defer_to_dead Let it be to be managed by dead properties DB.
 * @return          NULL on success.
 */
static dav_error *dav_ns_patch_validate(const dav_resource *resource,
        const apr_xml_elem *elem, int operation, void **context,
        int *defer_to_dead)
{
    dav_elem_private *priv = elem->priv;
    dav_ns_liveprop_context *value;
    const apr_text *data;
    int i;
    dmlite_context *ctx = resource->info->ctx;
    struct tm st_time;
    dav_error *err;

    /* Removing these properties is not supported */
    if (operation != DAV_PROP_OP_SET)
        return dav_shared_new_error(resource->info->request, NULL,
                HTTP_FORBIDDEN, "Live properties can not be removed (%s:%s)",
                dav_ns_namespace_uris[elem->ns], elem->name);

    /* Get pointer to the value */
    data = elem->first_cdata.first;
    if (!data)
        return dav_shared_new_error(resource->info->request, NULL,
                HTTP_CONFLICT, "No value specified for the property");

    /* Create context */
    value = apr_pcalloc(resource->pool, sizeof(dav_ns_liveprop_context));
    *context = value;

    /* Check the value is right */
    switch (priv->propid) {
        case DAV_PROPID_getlastmodified:
        case DAV_PROPID_lastaccess:
            if (strptime(data->text, "%a, %d %b %Y %H:%M:%S", &st_time) == NULL)
                return dav_shared_new_error(resource->info->request, NULL,
                        HTTP_CONFLICT, "Invalid date and time format");
            value->tim = timegm(&st_time);
            break;

        case DAV_PROPID_executable:
        case DAV_PROPID_executable_apache:
            /* We want a single letter: T or F */
            value->flag = data->text[0];
            if (strlen(data->text) != 1
                    || (value->flag != 'T' && value->flag != 'F'))
                return dav_shared_new_error(resource->info->request, NULL,
                        HTTP_CONFLICT,
                        "The 'executable' property expects only 'T' or 'F'");
            break;

        case DAV_PROPID_mode:
            /* Only a number */
            value->mode = strtol(data->text, NULL, 0);
            if (value->mode == 0 && data->text[0] != '0')
                return dav_shared_new_error(resource->info->request, NULL,
                        HTTP_CONFLICT, "A numeric string must be provided. "
                                "Tailing non-digit will be ignored");
            break;

        case DAV_PROPID_owner:
            for (i = 0; isdigit(data->text[i]); ++i)
                ;

            if (data->text[i] == '\0') {
                value->uid = atoi(data->text);
            }
            else {
                if (dmlite_getusrbynam(ctx, (char*) data->text, &value->uid)
                        != 0)
                    return dav_shared_new_error(resource->info->request, ctx,
                            HTTP_INTERNAL_SERVER_ERROR,
                            "Could not map the user %s", data->text);
            }
            break;

        case DAV_PROPID_group:
            for (i = 0; isdigit(data->text[i]); ++i)
                ;

            if (data->text[i] == '\0') {
                value->gid = atoi(data->text);
            }
            else {
                if (dmlite_getgrpbynam(ctx, (char*) data->text, &value->gid)
                        != 0)
                    return dav_shared_new_error(resource->info->request, ctx,
                            HTTP_INTERNAL_SERVER_ERROR,
                            "Could not map the group %s", data->text);
            }
            break;

        case DAV_PROPID_comment:
            /* The value is not empty, so we are good */
            value->string = data->text;
            break;

        case DAV_PROPID_replicas:
            if (resource->info->s_conf->type != DAV_NS_NODE_LFC) {
                return dav_shared_new_error(resource->info->request, ctx,
                        HTTP_METHOD_NOT_ALLOWED, "Can not modify replicas");
            }

            err = dav_ns_deserialize_replicas(resource->info->request,
                    data->text, &value->replica_array);
            if (err != NULL )
                return err;

            /* Validate action and values */
            for (i = 0; i < value->replica_array.nreplicas; ++i) {
                dmlite_replica *replica = &value->replica_array.replicas[i];

                switch (value->replica_array.action[i]) {
                    case '+':
                    case '-':
                    case 'M':
                        break;
                    default:
                        return dav_shared_new_error(resource->info->request,
                                NULL, HTTP_BAD_REQUEST,
                                "Action '%c' for replica not supported",
                                value->replica_array.action[i]);
                }

                if (replica->rfn[0] == '\0')
                    return dav_shared_new_error(resource->info->request, NULL,
                            HTTP_BAD_REQUEST,
                            "The attribute 'replica' is empty or not defined");
            }

            break;

        case DAV_PROPID_guid:
            value->string = data->text;
            break;

        case DAV_PROPID_xattr:
            value->dict = dmlite_any_dict_from_json(data->text);
            if (value->dict == NULL )
                return dav_shared_new_error(resource->info->request, NULL,
                        HTTP_BAD_REQUEST, "Malformed JSON");
            break;

        default:
            /* Unsupported */
            *defer_to_dead = 0;
            return dav_shared_new_error(resource->info->request, NULL,
                    HTTP_FORBIDDEN,
                    "Unknown live property and dead properties not supported");
    }

    return NULL ;
}

/**
 * Finally, set the property.
 * @param resource  The resource being modified.
 * @param elem      The value of the property.
 * @param operation Set or remove.
 * @param context   As set by dav_ns_patch_validate.
 * @param rollback_ctx In case the change has to be undone, put here relevant information.
 * @return          NULL on success.
 */
static dav_error *dav_ns_patch_exec(const dav_resource *resource,
        const apr_xml_elem *elem, int operation, void *context,
        dav_liveprop_rollback **rollback_ctx)
{
    (void) operation;
    (void) rollback_ctx;

    dav_elem_private *priv = elem->priv;
    dav_ns_liveprop_context *value = context;
    int n;
    dmlite_context *ctx = resource->info->ctx;
    struct utimbuf timebuf;

    /* We already have the proper values in *value (*context) */
    switch (priv->propid) {
        case DAV_PROPID_getlastmodified:
            timebuf.modtime = value->tim;
            timebuf.actime = resource->info->stat.stat.st_atime;
            if (dmlite_utime(ctx, resource->info->sfn, &timebuf) != 0)
                return dav_shared_new_error(resource->info->request, ctx, 0,
                        "Could not change mtime of %s", resource->info->sfn);
            break;

        case DAV_PROPID_lastaccess:
            timebuf.modtime = resource->info->stat.stat.st_mtime;
            timebuf.actime = value->tim;
            if (dmlite_utime(ctx, resource->info->sfn, &timebuf) != 0)
                return dav_shared_new_error(resource->info->request, ctx, 0,
                        "Could not change atime of %s", resource->info->sfn);
            break;

        case DAV_PROPID_executable:
        case DAV_PROPID_executable_apache:
            /* Set the executable flag and let the next case do the job */
            if (value->flag == 'T')
                value->mode = resource->info->stat.stat.st_mode | S_IXUSR;
            else
                value->mode = resource->info->stat.stat.st_mode & ~S_IXUSR;

        case DAV_PROPID_mode:
            if (dmlite_chmod(ctx, resource->info->sfn, value->mode) != 0)
                return dav_shared_new_error(resource->info->request, ctx, 0,
                        "Could not change the mode");
            break;

        case DAV_PROPID_owner:
            if (dmlite_chown(ctx, resource->info->sfn, value->uid, -1) != 0)
                return dav_shared_new_error(resource->info->request, ctx, 0,
                        "Could not change the uid");
            break;

        case DAV_PROPID_group:
            if (dmlite_chown(ctx, resource->info->sfn, -1, value->gid) != 0)
                return dav_shared_new_error(resource->info->request, ctx, 0,
                        "Could not change the gid");
            break;

        case DAV_PROPID_comment:
            if (dmlite_setcomment(ctx, resource->info->sfn, value->string) != 0)
                return dav_shared_new_error(resource->info->request, ctx, 0,
                        "Could not set the comment");
            break;

        case DAV_PROPID_replicas:
            for (n = 0; n < value->replica_array.nreplicas; ++n) {
                dmlite_replica* replica = &value->replica_array.replicas[n];
                replica->fileid = resource->info->stat.stat.st_ino;

                switch (value->replica_array.action[n]) {
                    case '+':
                        if (dmlite_addreplica(ctx, replica) != 0)
                            return dav_shared_new_error(resource->info->request,
                                    ctx, 0, "Could not add the replica %s",
                                    replica->rfn);
                        break;
                    case '-':
                        if (dmlite_delreplica(ctx, replica) != 0)
                            return dav_shared_new_error(resource->info->request,
                                    ctx, 0, "Could not delete the replica %s",
                                    replica->rfn);
                        break;
                    default:
                        dmlite_updatereplica(ctx, replica);
                        break;
                }

                /* We are done with this, so free */
                if (replica->extra != NULL )
                    dmlite_any_dict_free(replica->extra);
            }
            break;

        case DAV_PROPID_guid:
            if (dmlite_setguid(ctx, resource->info->sfn, value->string) != 0)
                return dav_shared_new_error(resource->info->request, ctx, 0,
                        "Could not update the Grid Unique Identifier");
            break;

        case DAV_PROPID_xattr:
            n = dmlite_update_xattr(ctx, resource->info->sfn, value->dict);
            dmlite_any_dict_free(value->dict);
            if (n != 0)
                return dav_shared_new_error(resource->info->request, ctx, 0,
                        "Could not update the extended attributes");
            break;

        default:
            /* We will never enter here due to previous checking */
            break;
    }

    return NULL ;
}

/**
 * Nothing to do here, really.
 * @param resource
 * @param operation
 * @param context
 * @param rollback_ctx
 */
static void dav_ns_patch_commit(const dav_resource *resource, int operation,
        void *context, dav_liveprop_rollback *rollback_ctx)
{
    (void) resource;
    (void) operation;
    (void) context;
    (void) rollback_ctx;
    return;
}

/**
 * Undo a change when a failure occurs.
 * @param resource  The resource being modified.
 * @param operation The operation performed (set, delete...)
 * @param context   As set by dav_ns_patch_validate.
 * @param rollback_ctx The rollback context.
 * @return          NULL on success.
 */
static dav_error *dav_ns_patch_rollback(const dav_resource *resource,
        int operation, void *context, dav_liveprop_rollback *rollback_ctx)
{
    (void) resource;
    (void) operation;
    (void) context;
    (void) rollback_ctx;
    /* If setting the property failed, it wasn't set at all, so there is
     * no need to rollback */
    return NULL ;
}

/** Properties handlers */
static const dav_hooks_liveprop dav_hooks_liveprop_dpm = {
        dav_ns_insert_prop, dav_ns_is_writable, dav_ns_namespace_uris,
        dav_ns_patch_validate, dav_ns_patch_exec, dav_ns_patch_commit,
        dav_ns_patch_rollback, NULL };

/** Properties */
const dav_liveprop_group dav_ns_liveprop_group = {
        dav_ns_props, /* Properties specification */
        dav_ns_namespace_uris, /* List of namespaces       */
        &dav_hooks_liveprop_dpm /* Handlers                 */
};

/**
 * Find the property ID so the value can be retrieved later.
 * @param resource The resource being processed.
 * @param ns       The property namespace (e.g. DAV:)
 * @param name     The property name      (e.g. getcontentlength)
 * @param hooks    Where to put the liveprop hooks.
 * @return         The property ID, or 0 if not found.
 */
int dav_ns_find_liveprop(const dav_resource *resource, const char *ns,
        const char *name, const dav_hooks_liveprop **hooks)
{
    const dav_liveprop_spec *scan;
    int i;

    /* One of us, not one of them */
    if (resource->hooks != &dav_ns_hooks_repository)
        return 0;

    /* Locate the namespace in the namespace table */
    for (i = 0; dav_ns_namespace_uris[i] != NULL ; ++i) {
        if (strcmp(ns, dav_ns_namespace_uris[i]) == 0)
            break;
    }

    /* No one matched? */
    if (dav_ns_namespace_uris[i] == NULL ) {
        ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, resource->info->request,
                "Property not found: %s:%s (unknown namespace)", ns, name);
        return 0;
    }

    /* Now look for the property ID */
    for (scan = dav_ns_props; scan->name != NULL ; ++scan) {
        if (i == scan->ns && strcmp(name, scan->name) == 0) {
            *hooks = &dav_hooks_liveprop_dpm;
            return scan->propid;
        }
    }

    /* Property name not found */
    ap_log_rerror(APLOG_MARK, APLOG_DEBUG, 0, resource->info->request,
            "Property not found: %s:%s", ns, name);
    return 0;
}

/**
 * Insert all supported liveprops into the output.
 * @param r        The request.
 * @param resource The resource being processed.
 * @param what     What to insert.
 * @param phdr     Where to insert.
 */
void dav_ns_insert_all_liveprops(request_rec *r, const dav_resource *resource,
        dav_prop_insert what, apr_text_header *phdr)
{
    (void) r;

    const dav_liveprop_spec *scan;

    /* One of us, not one of them */
    if (resource->hooks != &dav_ns_hooks_repository)
        return;

    /* Non-existing have no properties */
    if (!resource->exists)
        return;

    /* Insert all liveprops */
    for (scan = dav_ns_props; scan->name != NULL ; ++scan) {
        /* Hack: Avoid comments and replicas and others on full listings as
         * it is heavy! */
        switch (scan->propid) {
            case DAV_PROPID_comment:
            case DAV_PROPID_replicas:
            case DAV_PROPID_acl:
            case DAV_PROPID_supported_privilege_set:
            case DAV_PROPID_quota_free:
                break;
            default:
                dav_ns_insert_prop(resource, scan->propid, what, phdr);
        }
    }

    return;
}
