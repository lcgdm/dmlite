/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2011-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DELIVERY_H_
#define	DELIVERY_H_

#include "mod_lcgdm_ns.h"

dav_error *dav_ns_deliver_collection(const dav_resource *resource,
        ap_filter_t *output_filters, apr_bucket_brigade *bb);

dav_error *dav_ns_deliver_metalink(const dav_resource *resource,
        ap_filter_t *output_filters, apr_bucket_brigade *bb);

dav_error *dav_ns_deliver_virtual(const dav_resource *resource,
        ap_filter_t *output_filters, apr_bucket_brigade *bb);

dav_error *dav_ns_deliver_s3chunks(const dav_resource *resource,
        ap_filter_t *output_filters, apr_bucket_brigade *bb);

#endif	/* DELIVERY_H_ */

