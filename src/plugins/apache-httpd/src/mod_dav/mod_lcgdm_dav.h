/*
 * Dirty trick to assure our mod_dav.h is used
 */
#ifndef _MOD_LCGDM_DAV_H_
#define _MOD_LCGDM_DAV_H_

#include "mod_dav.h"

#define LCGDM_DAV_NOTE_COPY_KEY      "lcgdm.copy.supported"
#define LCGDM_DAV_NOTE_COPY_EXTERNAL "external"

#endif
