# Class defining the ESCAPE VO, as seen by the VOMS service.
#
# Takes care of all the required setup to enable access to the ESCAPE VO
# (users and services) in a grid enabled machine.
#
# == Examples
# 
# Simply enable this class:
#   class{'::voms::escape':}

class voms::escape {
  voms::client{'escape':
    servers => [
      {
        server => 'voms-escape.cloud.cnaf.infn.it',
        port   => '15000',
        dn     => '/DC=org/DC=terena/DC=tcs/C=IT/ST=Roma/O=Istituto Nazionale di Fisica Nucleare - INFN/OU=CNAF/CN=voms-escape.cloud.cnaf.infn.it',
        ca_dn  => '/C=NL/O=GEANT Vereniging/CN=GEANT eScience SSL CA 4',
      },
    ],
  }
}

