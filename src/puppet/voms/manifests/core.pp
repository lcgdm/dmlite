# === Define: voms::core
#
# Installs and configure a voms core server
#
# === Parameters
#
# [*vo*]
#  The name of the virtial organisation. Defaults to the namevar of the
#  the  voms::core instance.
#  
# [*issuer*]
#  The hostname of issuer of voms credentials. By default it is the $::fqdn.
# 
# [*vomstimeout*]
#  The timeout of voms proxy, by default 86400 seconds or 1 day.
#
# [*sqlhost*, *sqldbname*, *sqlport*, *sqlusername*, *sqlpw*]
#  The SQL server parameters, see default values below.
#
# [*port*]
#  The port the voms server should listen on.
#
# === Examples
# 
#   voms::core{'examplevo':
#        issuser => 'voms.example.org',
#        sqlpwd  => '12345',
#        sqlhost => 'mysql.example.org.'
#   }
#
# === Authors
# Steve Traylen <steve.traylen@cern.ch>
# 
# === Copyright 
# Copyright Steve Traylen,  CERN 2012 
# 
# === License
#  Apache II
# 

define voms::core(
  $sqlpwd,
  $port,
  $vo=$name,
  $guid=$name,
  $issuer=$::fqdn,
  $vomstimeout='86400',
  $sqlhost='localhost',
  $sqldbname="${vo}_db",
  $sqlport=3306,
  $sqlusername="${vo[0,7]}_core",
  $maxreqs='300',
  $passfile="/etc/voms/${vo}/voms.pass",
) {

  include("voms::${vo}")
  include('::voms::core::install')
  include('::voms::core::config')

  firewall {"100 allow ${vo} access from the universe.":
    proto  => 'tcp',
    dport  => $port,
    action => 'accept',
  }

  firewall {"100 ipv6 allow ${vo} access from the universe.":
    provider => 'ip6tables',
    proto    => 'tcp',
    dport    => $port,
    action   => 'accept',
  }

  file{"/etc/voms/${vo}":
    ensure  => directory,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    purge   => true,
    recurse => true,
    require => File['/etc/voms'],
  }

  file{"/etc/voms/${vo}/voms.conf":
    ensure  => file,
    content => template('voms/voms.conf.erb'),
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    require => File["/etc/voms/${vo}"],
  }
  # The package creates the voms user below.
  file{"/etc/voms/${name}/voms.pass":
    ensure  => file,
    content => "${sqlpwd}\n",
    mode    => '0640',
    owner   => 'voms',
    group   => 'voms',
    require => [File["/etc/voms/${vo}"],Package['voms-server']],
  }

  service{"voms@${vo}.service":
    ensure => true,
    enable => true,
  }

  @@mysql_user{"${sqlusername}@${::fqdn}":
    tag           => 'voms_database_users',
    password_hash => mysql_password($sqlpwd),
    require       => Class['mysql::server'],
  }
  @@mysql_grant{"${sqlusername}@${::fqdn}/${sqldbname}":
    tag        => 'voms_database_grant',
    privileges => ['SELECT'],
  }
}
