# == Define: voms::client

# Configures a VOMS client with the two files 
# /etc/vomses/... as used by voms-proxy-init.
# /etc/grid-security/vomsdir/... as used 
# == Parameters
#
# [*vo*]
#   The name of the VO, if no *vo* is specifed
#   the name of the instance will be used.

# [*servers*]
#   An array of hashes. For each VOMS server 
#   specify server name (server), its port (port), 
#   its distinguished name (dn) and it's certificate 
#   authority distinguished name (ca_dn) as keys to the hash.

# == Example
#
#     voms::client{'ops':
#        servers  => [{server => 'voms.cern.ch',
#                   port   => '15009',
#                   dn    => '/DC=ch/DC=cern/OU=computers/CN=voms.cern.ch',
#                   ca_dn => '/DC=ch/DC=cern/CN=CERN Trusted Certification Authority'
#                  },
#                  {server => 'lcg-voms.cern.ch',
#                   port   => '15009',
#                   dn    => '/DC=ch/DC=cern/OU=computers/CN=lcg-voms.cern.ch',
#                   ca_dn => '/DC=ch/DC=cern/CN=CERN Trusted Certification Authority'
#                  }]
#
# == Authors
#
# CERN IT/GT/DMS <it-dep-gt-dms@cern.ch>
# CERN IT/PES/PS <it-dep-pes-ps@cern.ch>
#
define voms::client ($vo = $name, $servers = []  ) {
  include ::voms::install
  Class['voms::install'] -> Voms::Client[$vo]

  file {"/etc/grid-security/vomsdir/${vo}":
    ensure  => directory,
    owner   => root,
    group   => root,
    mode    => '0755',
    recurse => true,
    purge   => true,
    require => File['/etc/grid-security/vomsdir'],
  }

  # Set defaults for the rest of this scope.
  File{
    owner => root,
    group => root,
    mode  => '0644',
  }

  $servers.each | $_s | {

    file{"/etc/grid-security/vomsdir/${vo}/${_s['server']}.lsc":
      ensure  => file,
      content => "${_s['dn']}\n${_s['ca_dn']}\n",
    }
    file{"/etc/vomses/${vo}-${_s['server']}":
      ensure  => file,
      content => "\"${vo}\" \"${_s['server']}\" \"${_s['port']}\" \"${_s['dn']}\" \"${vo}\" \"24\"\n",
    }
  }

  # Interim solution for trusting IAM VOMS endpoints only
  $iam_vos = ['alice', 'lhcb']
  if $vo in $iam_vos {
    file{"/etc/grid-security/vomsdir/${vo}/voms-${vo}-auth.app.cern.ch.lsc":
      ensure  => file,
      content => "/DC=ch/DC=cern/OU=computers/CN=${vo}-auth.web.cern.ch\n/DC=ch/DC=cern/CN=CERN Grid Certification Authority\n",
    }
  }
}
