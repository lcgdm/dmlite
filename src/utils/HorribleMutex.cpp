
#include "HorribleMutex.h"

// This is a horrible workaround in order to have a global mutex that serializes
// certain questionable boost functions, e.g. read_json
boost::mutex horribleboostmtx;

