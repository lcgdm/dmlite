cmake_minimum_required (VERSION 3.6)

# Find out details if we're on a redhat style platform
execute_process (COMMAND rpm -E "%{?fedora}" OUTPUT_VARIABLE FEDORA OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process (COMMAND rpm -E "%{?rhel}" OUTPUT_VARIABLE RHEL OUTPUT_STRIP_TRAILING_WHITESPACE)

# Build py2 except on Fedora > 30 and RedHat > 7
if(NOT DEFINED BUILD_PY2)
  if((NOT FEDORA GREATER 30) AND (NOT RHEL GREATER 7))
    set(BUILD_PY2 True)
  endif()
endif()

# Build py3 except on SL6
if(NOT DEFINED BUILD_PY3)
  if(NOT RHEL STREQUAL "6")
    set(BUILD_PY3 True)
  endif()
endif()

# Build Python2 pydmlite
  
if(BUILD_PY2)

  message(STATUS "Building Py2")

  if(RHEL STREQUAL "6")
    find_package(PythonLibs)
    execute_process (COMMAND python2 -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)"
                   OUTPUT_VARIABLE Python2_SITEARCH
                   OUTPUT_STRIP_TRAILING_WHITESPACE)
    # Set the FindPython2 style vars for later on
    set(Python2_INCLUDE_DIRS ${PYTHON_INCLUDE_DIRS})
    set(Python2_LIBRARIES ${PYTHON_LIBRARIES})
  else()
    find_package(Python2 COMPONENTS Interpreter Development)
  endif()

  if(RHEL STREQUAL "6")
    set(BOOST_PYTHON2 "")
  elseif(RHEL STREQUAL "7")
    set(BOOST_PYTHON2 "")
  else()
    set(BOOST_PYTHON2 "${Python2_VERSION_MAJOR}${Python2_VERSION_MINOR}")
  endif()

  find_package(Boost COMPONENTS "python${BOOST_PYTHON2}" REQUIRED)

  add_library(pydmlite 
              SHARED pydmlite.cpp authn.cpp base.cpp catalog.cpp errno.cpp 
                     exceptions.cpp extensible.cpp inode.cpp io.cpp 
                     pooldriver.cpp poolmanager.cpp types.cpp)

  IF(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
    target_link_libraries(pydmlite ${Boost_LIBRARIES} ${Python2_LIBRARIES} dmlite)
  ELSE()
    target_link_libraries(pydmlite ${Boost_LIBRARIES} dmlite)
  ENDIF(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")

  target_include_directories(pydmlite PUBLIC ${Boost_INCLUDE_DIRS} ${Python2_INCLUDE_DIRS})

  if(RHEL STREQUAL "6")
    link_directories(${Boost_LIBRARY_DIRS} ${CMAKE_BINARY_DIR}/src)
  else()
    target_link_directories(pydmlite PUBLIC ${Boost_LIBRARY_DIRS} ${CMAKE_BINARY_DIR}/src)
  endif()

  set_target_properties (pydmlite PROPERTIES PREFIX "")
  set_target_properties (pydmlite PROPERTIES LIBRARY_OUTPUT_DIRECTORY "py2")

  install(TARGETS       pydmlite
          LIBRARY       DESTINATION ${Python2_SITEARCH}
          PERMISSIONS   OWNER_EXECUTE OWNER_WRITE OWNER_READ
                        GROUP_EXECUTE GROUP_READ
                        WORLD_EXECUTE WORLD_READ
  )
endif()

# Build Python3 pydmlite

if(BUILD_PY3)
  message(STATUS "Building Py3")

  if(RHEL STREQUAL "8")
    find_package(PythonLibs)
    execute_process (COMMAND python3 -c "import sysconfig; print(sysconfig.get_path('platlib'))"
                   OUTPUT_VARIABLE Python3_SITEARCH
                   OUTPUT_STRIP_TRAILING_WHITESPACE)
    # Set the FindPython3 style vars for later on
    set(Python3_INCLUDE_DIRS ${PYTHON_INCLUDE_DIRS})
    set(Python3_LIBRARIES ${PYTHON_LIBRARIES})
  else()
    find_package(Python3 COMPONENTS Interpreter Development)
  endif()

  if((RHEL STREQUAL "7") OR (RHEL STREQUAL "8"))
    set(BOOST_PYTHON3 "3")
  else()
    set(BOOST_PYTHON3 "${Python3_VERSION_MAJOR}${Python3_VERSION_MINOR}")
  endif()

  find_package(Boost COMPONENTS "python${BOOST_PYTHON3}" REQUIRED)

  add_library(py3dmlite
              SHARED pydmlite.cpp authn.cpp base.cpp catalog.cpp errno.cpp 
                     exceptions.cpp extensible.cpp inode.cpp io.cpp 
                     pooldriver.cpp poolmanager.cpp types.cpp)

  target_link_libraries(py3dmlite ${Boost_LIBRARIES} ${Python3_LIBRARIES} dmlite)
  target_include_directories (py3dmlite PUBLIC ${Boost_INCLUDE_DIRS} ${Python3_INCLUDE_DIRS})
  if(RHEL STREQUAL "8")
    link_directories(${Boost_LIBRARY_DIRS} ${CMAKE_BINARY_DIR}/src)
  else()
    target_link_directories(py3dmlite PUBLIC ${Boost_LIBRARY_DIRS} ${CMAKE_BINARY_DIR}/src)
  endif()

  set_target_properties (py3dmlite PROPERTIES PREFIX "")
  set_target_properties (py3dmlite PROPERTIES OUTPUT_NAME "pydmlite")
  set_target_properties (py3dmlite PROPERTIES LIBRARY_OUTPUT_DIRECTORY "py3")

  install(TARGETS       py3dmlite
          LIBRARY       DESTINATION ${Python3_SITEARCH}
          PERMISSIONS   OWNER_EXECUTE OWNER_WRITE OWNER_READ
                        GROUP_EXECUTE GROUP_READ
                        WORLD_EXECUTE WORLD_READ
          RENAME        pydmlite.so
  )
endif()
