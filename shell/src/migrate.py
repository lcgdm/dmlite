# Script that create DPM namespace and config dumps usable for migration
# to different storage implementation.
#
# Petr Vokac, 2021/05/30
#
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division

import os, sys, time, re, io
import logging, logging.handlers
import copy
import stat
import socket
import select
import csv
import uuid
import errno
import pwd, grp

try: import simplejson as json
except ImportError: import json

# avoid additional dependencies and import database modules only when necessary
# * mysql module MySQLdb/pymysql only for DPM export
# * ssh client paramiko module only for DPM export
# * postgresql module psycopg2 only for dCache import

__version__ = '0.0.1'

_log = logging.getLogger('dmlite-shell')


class dpm(object):

    def __init__(self, conn_cns=None, conn_dpm=None):
        self._conn_cns = conn_cns
        self._conn_dpm = conn_dpm
        self._user_cache = None
        self._group_cache = None
        self._link_cache = None
        self._spacetoken_cache = None
        self._path_cache = None
        self._host_fs_space = {}


    def _group_cleanup(self, name):
        if name.endswith('/Capability=NULL'):
            name = name[:-len('/capability=NULL')]
        if name.endswith('/Role=NULL'):
            name = name[:-len('/Role=NULL')]
        return "/{0}".format(name)


    def _load_uid(self):
        ret = {
            0: 'root', # default mapping
        }

        cursor = self._conn_cns.cursor()
        try:
            sql = 'SELECT `userid`, `username` FROM Cns_userinfo ORDER BY rowid'
            cursor.execute(sql)
            for row in cursor:
                uid, name = row
                ret[uid] = name
        finally:
            cursor.close()

        return ret


    def _map_uid(self, uid):
        if self._user_cache == None:
            self._user_cache = self._load_uid()

        return self._user_cache.get(uid, "unknown_uid_{0}".format(uid))


    def _load_gid(self):
        ret = {
            0: 'root', # default mapping
        }

        cursor = self._conn_cns.cursor()
        try:
            sql = 'SELECT `gid`, `groupname` FROM Cns_groupinfo ORDER BY rowid'
            cursor.execute(sql)
            for row in cursor:
                gid, name = row
                ret[gid] = self._group_cleanup(name)
        finally:
            cursor.close()

        return ret


    def _map_gid(self, gid):
        if self._group_cache == None:
            self._group_cache = self._load_gid()

        return self._group_cache.get(gid, "unknown_gid_{0}".format(gid))


    def _load_link(self):
        ret = {}

        cursor = self._conn_cns.cursor()
        try:
            sql = 'SELECT fileid, linkname FROM Cns_symlinks'
            cursor.execute(sql)
            for row in cursor:
                fileid, target = row
                ret[fileid] = target.decode('utf-8')
        finally:
            cursor.close()

        return ret


    def _map_link(self, fileid):
        if self._link_cache == None:
            self._link_cache = self._load_link()

        return self._link_cache.get(fileid)


    def _load_spacetokens(self):
        setname2spacetoken = {}
        path2spacetoken = {}
        path2setname = {}
        dupl = {}

        cursor = self._conn_dpm.cursor()
        try:
            #sql = 'SELECT `s_token`, `u_token`, `dpm_space_reserv`.`poolname`, `server`, `fs` FROM dpm_space_reserv JOIN dpm_fs ON `dpm_space_reserv`.`poolname` = `dpm_fs`.`poolname` ORDER BY `s_token`, `server`, `fs`'
            sql = 'SELECT `path`, `s_token`, `u_token` FROM dpm_space_reserv'
            cursor.execute(sql)
            for row in cursor:
                path, setname, spacetoken = row
                if spacetoken in [None, '']:
                    spacetoken = 'EMPTY'

                setname2spacetoken[setname] = spacetoken
                dupl.setdefault(spacetoken, []).append(setname)
                if path not in [None, '']:
                    path2setname[path] = setname

        finally:
            cursor.close()

        # avoid duplicate spacetoken names
        for spacetoken, setnames in dupl.items():
            if len(setnames) == 1: continue
            for setname in setnames:
                setname2spacetoken[setname] = "{0}-{1}".format(setname2spacetoken[setname], setname)

        for path, setname in path2setname.items():
            path2spacetoken[path] = setname2spacetoken[setname]

        return (setname2spacetoken, path2spacetoken)


    def _map_spacetokens(self, setname, path=None):
        if self._spacetoken_cache == None:
            self._spacetoken_cache, self._path_cache = self._load_spacetokens()

        if setname != None and setname in self._spacetoken_cache:
            return self._spacetoken_cache[setname]

        if path == None:
            path = '/'

        maxlen = 0
        path_spacetoken = None
        for apath, spacetoken in self._path_cache.items():
            if maxlen < len(apath) and (path == apath or path.startswith("{0}/".format(apath))):
                maxlen = len(apath)
                path_spacetoken = spacetoken

        if path_spacetoken == None:
            #raise Exception("no spacetoken path defined, migrating legacy DPM is not supported")
            _log.debug("top level directory %s with no spacetoken", path)
            return ''

        return path_spacetoken


    def _get_users(self, used=False):
        uid2name = {
            0: 'root', # default mapping
        }

        cursor = self._conn_cns.cursor()
        try:
            if not used:
                sql = 'SELECT userid, username FROM Cns_userinfo ORDER BY rowid'
            else:
                sql = 'SELECT owner_uid, username FROM (SELECT DISTINCT owner_uid FROM Cns_file_metadata) AS tmp LEFT JOIN Cns_userinfo ON tmp.owner_uid = Cns_userinfo.userid ORDER BY rowid'
            cursor.execute(sql)
            for row in cursor:
                uid, name = row
                if name != None:
                    uid2name[uid] = name
                elif uid not in uid2name:
                    uid2name[uid] = "unknown_uid_{0}".format(uid)
        finally:
            cursor.close()

        return uid2name


    def _get_groups(self, used=False):
        gid2name = {
            0: 'root', # default mapping
        }

        cursor = self._conn_cns.cursor()
        try:
            if not used:
                sql = 'SELECT gid, groupname FROM Cns_groupinfo ORDER BY rowid'
            else:
                sql = 'SELECT tmp.gid, groupname FROM (SELECT DISTINCT gid FROM Cns_file_metadata) AS tmp LEFT JOIN Cns_groupinfo ON tmp.gid = Cns_groupinfo.gid ORDER BY rowid'
            cursor.execute(sql)
            for row in cursor:
                gid, name = row
                if name != None:
                    gid2name[gid] = self._group_cleanup(name)
                elif gid not in gid2name:
                    gid2name[gid] = "unknown_gid_{0}".format(gid)
        finally:
            cursor.close()

        return gid2name


    def _get_links(self):
        fileid2target = {}

        cursor = self._conn_cns.cursor()
        try:
            sql = 'SELECT fileid, linkname FROM Cns_symlinks'
            cursor.execute(sql)
            for row in cursor:
                fileid, target = row
                fileid2target[fileid] = target
        finally:
            cursor.close()

        return fileid2target


    def _get_space_configuration(self):
        cursor = self._conn_dpm.cursor()
        try:
            project_host_fss = {}
            sql = 'SELECT `poolname`, `server`, `fs` FROM dpm_fs'
            cursor.execute(sql)
            for project, host, fs in cursor:
                project_host_fss.setdefault(project, []).append((host, fs))

            #sql = 'SELECT `dpm_space_reserv`.`poolname`, `u_token`, `path`, `server`, `fs` FROM dpm_space_reserv JOIN dpm_fs ON `dpm_space_reserv`.`poolname` = `dpm_fs`.`poolname` ORDER BY `dpm_space_reserv`.`poolname`, `path`, `u_token`, `server`, `fs`'
            sql = 'SELECT `poolname`, `s_token`, `path`, `groups`, `t_space` FROM dpm_space_reserv ORDER BY `poolname`, `path`'
            cursor.execute(sql)
            for project, spacetoken_uuid, path, groups, space in cursor:
                spacetoken = self._map_spacetokens(spacetoken_uuid)
                group_names = [self._map_gid(int(x)) for x in groups.split(',') if x != '' and int(x) != 0]
                project_host_fs = project_host_fss.get(project, [])
                yield (project, spacetoken, path, group_names, space, project_host_fs)

        finally:
            cursor.close()


    def _get_fs_space(self, host, fs):
        if (host, fs) not in self._host_fs_space:
            try:
                self._host_fs_space[(host, fs)] = self._remote_fs_space(host, fs)
            except:
                self._host_fs_space[(host, fs)] = (0, 0)

        return self._host_fs_space[(host, fs)]

    def _remote_fs_space(self, server, fs):
        total = 0
        used = 0

        try:
            import paramiko
            # avoid warning "encode_point has been deprecated on EllipticCurvePublicNumbers"
            # (already fixed in paramiko 2.5, but supported OSes provides older version)
            import cryptography.utils
            if hasattr(cryptography.utils, 'CryptographyDeprecationWarning'):
                import warnings
                warnings.simplefilter("ignore", cryptography.utils.CryptographyDeprecationWarning)
        except ImportError:
            sys.exit("Could not import ssh library module. Please install the paramiko rpm.")

        try:
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(server, port=22, username='root', allow_agent=True, look_for_keys=True)

            channel = ssh.get_transport().open_session()
            channel.exec_command("df -B 1 -P {0}".format(fs))
            channel.shutdown_write()

            stdout_bytes = io.BytesIO()
            stderr_bytes = io.BytesIO()
            while not channel.eof_received or channel.recv_ready() or channel.recv_stderr_ready():
                readq, _, _ = select.select([channel], [], [], 30)
                for c in readq:
                    if c.recv_ready():
                        stdout_bytes.write(channel.recv(len(c.in_buffer)))
                    if c.recv_stderr_ready():
                        stderr_bytes.write(channel.recv_stderr(len(c.in_buffer)))

            exit_status = channel.recv_exit_status()
            channel.shutdown_read()
            channel.close()

            if exit_status != 0:
                raise Exception("ssh df exit status %i".format(exit_status))

            stdout_bytes.seek(0)
            stderr_bytes.seek(0)
            stdout_wrapper = io.TextIOWrapper(stdout_bytes, encoding='utf-8')
            stderr_wrapper = io.TextIOWrapper(stderr_bytes, encoding='utf-8')
            stdout_lines = stdout_wrapper.readlines()
            stderr_lines = stderr_wrapper.readlines()

            res = re.match(r"^(\S+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)%\s+(\S+)$", stdout_lines[1])
            total = int(res.group(2))
            used = int(res.group(3))

            return (total, used)

        except Exception as e:
            _log.error("could not ssh to %s for reading %s disk size: %s", server, fs, str(e))

            raise Exception("can't get %s:/%s size" % (server, fs))


    def _normalize_acls(self, acls, fileid=-1):
        if acls in [None, b'']:
            return []

        ret = []
        acllist = acls.split(b',')

        amask = 0o7 # no masking
        for a in acllist:
            atype = a[0] - ord(b'@')
            aperm = a[1] - ord(b'0')
            if atype == 5:
                amask = aperm

        for a in acllist:
            atype = a[0] - ord(b'@')
            aperm = a[1] - ord(b'0')
            aid = int(a[2:])

            adefault = atype & 0x20 == 0x20 # kDefault
            if atype & (0x20 ^ 0xff) in [1, 2]: # kUserObj, kUser
                auser = self._map_uid(aid)
                ret.append(('default_user' if adefault else 'user', aperm, auser))
            elif atype & (0x20 ^ 0xff) in [3, 4]: # kGroupObj, kGroup
                agroup = self._map_gid(aid)
                ret.append(('default_group' if adefault else 'group', aperm & amask, agroup))
            elif atype & (0x20 ^ 0xff) == 5: # kMask
                #ret.append(('default_mask' if adefault else 'mask', aperm, ''))
                pass
            elif atype & (0x20 ^ 0xff) == 6: # kOther
                ret.append(('default_other' if adefault else 'other', aperm, ''))
            else:
                _log.warn("unknown ACL entry type %i for fileid %i (acl entry: %s)", atype, fileid, a)

        return ret

    def _filter_acls(self, acls, user, group, mode):
        ret = []

        for acl in acls:
            atype, aperm, aname = acl

            if atype == 'user' and aperm == (0o7 & (mode >> 6)) and aname == user:
                continue
            if atype == 'group' and aperm == (0o7 & (mode >> 3)) and aname == group:
                continue
            if atype == 'other' and aperm == (0o7 & (mode >> 0)):
                continue

            ret.append(acl)

        return ret

    def _serialize_acls(self, acls):
        ret = []

        for acl in acls:
            atype, aperm, aname = acl

            amode = ''
            amode += '-' if aperm & stat.S_IROTH == 0 else 'r'
            amode += '-' if aperm & stat.S_IWOTH == 0 else 'w'
            amode += '-' if aperm & stat.S_IXOTH == 0 else 'x'

            ret.append((atype, amode, aname))
            #ret.append("{0}:{1}:{2}".format(atype, amode, aname))
            #if atype not in ['other', 'default other']:
            #    ret["{0}:{1}".format(atype, aname)] = amode
            #else:
            #    ret[atype] = amode

        return json.dumps(ret)

    def dump(self, skip_acl=False, skip_multi_replica=False):
        _log.info("dump started")

        csmap = {
            'AD': 'adler32',
            'CD': 'crc32',
            'MD': 'md5',
        }

        cursor = self._conn_cns.cursor()
        try:
            cnt_all = {}
            cnt_used = {}
            dir_rows = None

            # select top level directory object
            sql = 'SELECT fileid, name, owner_uid, gid, filemode, atime, ctime, mtime, acl FROM Cns_file_metadata WHERE parent_fileid = 0'
            cursor.execute(sql)
            if cursor.rowcount != 1:
                raise Exception("unable to find top level directory object")

            fileid, name, uid, gid, mode, atime, ctime, mtime, acl = cursor.fetchone()
            user = self._map_uid(uid)
            group = self._map_gid(gid)
            acllist = [] if skip_acl else self._filter_acls(self._normalize_acls(acl, fileid), user, group, mode)
            dir_rows = [(fileid, '', user, group, mode, atime, ctime, mtime, acllist, None)]

            # recursively walk through all sub directories
            while len(dir_rows) > 0:
                lastfileid = None
                lastcsumid = None
                lastcsums = None

                parentid, parentpath, puser, pgroup, pmode, patime, pctime, pmtime, pacllist, pspacetoken = dir_rows.pop()
                yield ('DIR', parentpath, puser, pgroup, pmode & 0o777, patime, pctime, pmtime, pacllist, pspacetoken, 0, None, None, None, None, None, None)

                sql = 'SELECT m.fileid, m.name, m.owner_uid, m.gid, m.filemode, m.atime, m.ctime, m.mtime, m.filesize, m.csumtype, m.csumvalue, m.acl, m.xattr, r.setname, r.poolname, r.host, r.fs, r.sfn, r.xattr FROM Cns_file_metadata AS m LEFT JOIN Cns_file_replica AS r ON m.fileid = r.fileid WHERE m.parent_fileid = %s'
                cursor.execute(sql, (parentid, ))
                for row in cursor:
                    fileid, name, uid, gid, mode, atime, ctime, mtime, size, csumtype, csumvalue, acl, mxattr, setname, pool, host, fs, sfn, rxattr = row

                    tobj = 'UNKNOWN'
                    if mode & stat.S_IFDIR == stat.S_IFDIR:
                        tobj = 'DIR'
                    elif mode & stat.S_IFLNK == stat.S_IFLNK:
                        tobj = 'LINK'
                    elif mode & stat.S_IFREG == stat.S_IFREG:
                        tobj = 'FILE'

                    cnt_all[tobj] = cnt_all.get(tobj, 0) + 1

                    if parentid == 0:
                        name = ''
                        path = ''
                    else:
                        path = "{0}/{1}".format(parentpath, name)

                    if lastfileid == fileid:
                        if skip_multi_replica:
                            _log.error("skipping additional replicas for fileid %i, path %s", fileid, path)
                            continue
                    else:
                        lastfileid = fileid

                    user = self._map_uid(uid)
                    group = self._map_gid(gid)
                    target = None

                    csums = None
                    if tobj == 'DIR':
                        #parentids.append((fileid, path))
                        pass

                    elif tobj == 'LINK':
                        target = self._map_link(fileid)
                        if target == None:
                            _log.error("ignoring link %s with missing target", path)
                            continue

                    elif tobj == 'FILE':

                        if mxattr not in [None, '', '{}']:
                            try:
                                mxattr = json.loads(mxattr)
                            except Exception as e:
                                _log.error("unable to parse file metadata xattr for fileid %i: %s", fileid, str(mxattr))
                                mxattr = {}
                        else:
                            mxattr = {}

                        if rxattr not in [None, '', '{}']:
                            try:
                                rxattr = json.loads(rxattr)
                            except Exception as e:
                                _log.error("unable to parse file metadata xattr for fileid %i: %s", fileid, str(rxattr))
                                rxattr = {}
                        else:
                            rxattr = {}

                        # legacy checksums
                        csums = {}
                        if csumtype in csmap.keys() and csumvalue != '':
                            csums[csmap[csumtype]] = csumvalue
                        # file metadata checksums
                        for k, v in mxattr.items():
                            if k.startswith('checksum.'):
                                csumtype = k[len('checksum.'):]
                                if csumtype in csums and csums[csumtype] != v:
                                    _log.error("legacy checksum doesn't match metadata checksum for fileid %i: %s x %s", fileid, csums[csumtype], v)
                                csums[csumtype] = v
                        # file replica checksums
                        for k, v in rxattr.items():
                            if k.startswith('checksum.'):
                                csumtype = k[len('checksum.'):]
                                if csumtype in csums and csums[csumtype] != v:
                                    _log.error("legacy or metadata checksum doesn't match replica checksum for fileid %i: %s x %s", fileid, csums[csumtype], v)
                                csums[csumtype] = v
                        # validate checksums for different replicas
                        if lastcsumid == fileid:
                            for csumtype, v in lastcsums.items():
                                if csumtype in csums and csums[csumtype] != v:
                                    _log.error("replicas metadata checksums doesn't match for fileid %i: %s x %s", fileid, csums[csumtype], v)
                        else:
                            lastcsumid = fileid
                            lastcsums = copy.deepcopy(csums)

                    acllist = [] if skip_acl else self._filter_acls(self._normalize_acls(acl, fileid), user, group, mode)
                    spacetoken = self._map_spacetokens(setname, path)

                    if sfn != None:
                        sfn = sfn.decode('utf-8')
                        hostfs = "{0}:{1}".format(host, fs)
                        if sfn.lower().startswith(hostfs.lower()):
                            pos = len(hostfs)
                            while len(sfn) > pos and sfn[pos] == '/':
                                pos += 1
                            sfn = sfn[pos:]
                        else:
                            _log.error("host and fs doesn't match sfn for fileid %i: %s:%s x %s", fileid, host, fs, sfn)

                    cnt_used[tobj] = cnt_used.get(tobj, 0) + 1

                    if tobj != 'DIR':
                        yield (tobj, path, user, group, mode & 0o777, atime, ctime, mtime, acllist, spacetoken, size, csums, target, pool, host, fs, sfn)
                    else:
                        dir_rows.append((fileid, path, user, group, mode & 0o777, atime, ctime, mtime, acllist, spacetoken))

        finally:
            cursor.close()

            _log.info("dump completed: dirs %i%s, files %i%s, links %i%s, unknown %i%s",
                      cnt_used.get('DIR', 0), '' if cnt_all.get('DIR', 0) == cnt_used.get('DIR', 0) else " (skipped {0})".format(cnt_all['DIR']-cnt_used.get('DIR', 0)),
                      cnt_used.get('FILE', 0), '' if cnt_all.get('FILE', 0) == cnt_used.get('FILE', 0) else " (skipped {0})".format(cnt_all['FILE']-cnt_used.get('FILE', 0)),
                      cnt_used.get('LINK', 0), '' if cnt_all.get('LINK', 0) == cnt_used.get('LINK', 0) else " (skipped {0})".format(cnt_all['LINK']-cnt_used.get('LINK', 0)),
                      cnt_used.get('UNKNOWN', 0), '' if cnt_all.get('UNKNOWN', 0) == cnt_used.get('UNKNOWN', 0) else " (skipped {0})".format(cnt_all['UNKNOWN']-cnt_used.get('UNKNOWN', 0)))


    def export_csv(self, nsfile, configfile, skip_acl):
        _log.info("migration started (namespace=%s, config=%s)", nsfile, configfile)

        used_groupuser = set()
        used_spacetokens = set()
        used_pools = set()
        used_hosts = set()
        used_fss = set()
        pshf = {}
        cnt = {}

        #user_paths = {}

        start_time_mono = time.monotonic()
        start_time_cpu = time.process_time()
        record_time_mono = start_time_mono
        record_time_cpu = start_time_cpu

        # get all data from dpm_db database here, because this connection is idle
        # for a long time and it seems some MySQL configuration can drop it
        # (we currently don't implement automatic reconnection)
        space_config = [x for x in self._get_space_configuration()]

        #with io.open(nsfile, 'w', encoding='utf-8', newline='') as f: # FIXME: csv writing with python 2.7
        with io.open(nsfile, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, quotechar="'", quoting=csv.QUOTE_MINIMAL)

            lastpath = None
            for tobj, path, user, group, mode, atime, ctime, mtime, acl, spacetoken, size, csums, target, pool, host, fs, sfn in self.dump(skip_acl=skip_acl):
                cnt['ALL'] = cnt.get('ALL', 0) + 1

                #_log.debug("%s %s %s %s %s", tobj, path, user, group, csums)
                if tobj == 'DIR':
                    writer.writerow([tobj, path, user, group, "0%o" % mode, self._serialize_acls(acl), atime, ctime, mtime, spacetoken])
                    cnt[tobj] = cnt.get(tobj, 0) + 1
                elif tobj == 'LINK':
                    writer.writerow([tobj, path, user, group, "0%o" % mode, self._serialize_acls(acl), atime, ctime, mtime, spacetoken, target])
                    cnt[tobj] = cnt.get(tobj, 0) + 1
                elif tobj == 'FILE':
                    if pool == None and host == None and fs == None:
                        _log.warn("Ignoring file %s with no replica (%s:%s)", path, user, group)
                    else:
                        writer.writerow([tobj, path, user, group, "0%o" % mode, self._serialize_acls(acl), atime, ctime, mtime, spacetoken, size, json.dumps(csums), pool, host, fs, sfn])
                        k = (pool, spacetoken, host, fs)
                        pshf[k] = pshf.get(k, 0) + size
                        cnt['REPLICA'] = cnt.get('REPLICA', 0) + 1
                    if lastpath != path:
                        cnt['FILE'] = cnt.get('FILE', 0) + 1
                    lastpath = path
                else:
                    _log.error("unsupported / invalid file object %s for %s", tobj, path)
                    cnt['UNKNOWN'] = cnt.get('UNKNOWN', 0) + 1

                if cnt['ALL'] % 100000 == 0:
                    record_time_mono_new = time.monotonic()
                    record_time_cpu_new = time.process_time()
                    _log.info("processed %i records in %.01fs (cpu %.01fs, eff %.01f%%): dirs %i, files %i (replicas %i), links %i, unknown %i",
                              cnt.get('ALL', 0), record_time_mono_new - record_time_mono, record_time_cpu_new - record_time_cpu,
                              0. if record_time_mono_new == record_time_mono else 100. * (record_time_cpu_new - record_time_cpu) / (record_time_mono_new - record_time_mono),
                              cnt.get('DIR', 0), cnt.get('FILE', 0), cnt.get('REPLICA', 0), cnt.get('LINK', 0), cnt.get('UNKNOWN', 0))
                    record_time_mono = record_time_mono_new
                    record_time_cpu = record_time_cpu_new

                used_groupuser.add((group, user))
                for atype, aperm, aname in acl:
                    if atype in ['user', 'default user']:
                        used_groupuser.add((group, aname))
                    if atype in ['group', 'default group']:
                        used_groupuser.add((aname, user))
                used_spacetokens.add(spacetoken)
                used_pools.add(pool)
                used_hosts.add(host)
                used_fss.add(fs)

                # this could give use better idea where individual users store their
                # data and this information could be later used to remap concrete users
                # to e.g. more generic VO production account
                #depth = 6
                #apath = path.split('/', depth)
                #if len(apath) == depth+1 and apath[0] == '' and apath[1] == 'dpm' and apath[3] == 'home':
                #    vopath = '/'.join(apath[:-1])
                #    user_paths.setdefault(user, {})[vopath] = user_paths.get(user, {}).get(vopath, 0) + 1

        with io.open(configfile, 'w', newline='') as f:
            writer = csv.writer(f, quotechar="'", quoting=csv.QUOTE_MINIMAL)

            writer.writerow(['headnode', socket.getfqdn()])

            for user in sorted(set([user for _, user in used_groupuser])):
                writer.writerow(['user', user])

            for group in sorted(set([group for group, _ in used_groupuser])):
                writer.writerow(['group', group])

            group2users = {}
            for group, user in sorted(used_groupuser):
                #writer.writerow(['groupuser', group, user])
                group2users.setdefault(group, []).append(user)
            for group, users in sorted(group2users.items()):
                _log.debug("group2users: %s -> %s", group, ", ".join(sorted(users)))

            fslist = set()
            projectcfg = {}
            spacetokencfg = {}
            for project, spacetoken, path, groups, space, hostfss in space_config:
                spacetokencfg[spacetoken] = (project, path, groups, space, hostfss)
                pspacetokens, pspace, phostfss = projectcfg.get(project, (set(), 0, set()))
                pspacetokens.add(spacetoken)
                for host, fs in hostfss:
                    phostfss.add((host, fs))
                    fslist.add((host, fs))
                    # assign used space for all host:/fs associated with project and spacetoken
                    if (project, spacetoken, host, fs) not in pshf:
                        pshf[(project, spacetoken, host, fs)] = 0
                projectcfg[project] = (pspacetokens, pspace+space, phostfss)

            projectused = {}
            spacetokenused = {}
            hostfs2project = {}
            hostfs2spacetoken = {}
            for k, used in sorted(pshf.items()):
                project, spacetoken, host, fs = k
                projectused[project] = projectused.get(project, 0) + used
                spacetokenused[spacetoken] = spacetokenused.get(spacetoken, 0) + used
                hostfs2project.setdefault((host, fs), set()).add(project)
                hostfs2spacetoken.setdefault((host, fs), set()).add(spacetoken)

            for host, fs in sorted(fslist):
                total, used = self._get_fs_space(host, fs)
                projects = hostfs2project.get((host, fs), [])
                spacetokens = hostfs2spacetoken.get((host, fs), [])
                writer.writerow(['filesystem', ','.join(projects), ','.join(spacetokens), host, fs, total, used])

            for spacetoken in sorted(spacetokencfg.keys()):
                project, path, groups, space, hostfss = spacetokencfg[spacetoken]
                used = spacetokenused.get(spacetoken, 0)
                if used > space:
                    _log.warn("spacetoken size %i is smaller than used space %i, automatically increased spacetoken size to the used space + 1GB", space, used)
                writer.writerow(['spacetoken', project, spacetoken, path, ','.join(groups), space if space > used else used + 1024**3, used])

            for project in sorted(projectcfg.keys()):
                spacetokens, space, hostfss = projectcfg[project]
                used = projectused.get(project, 0)
                writer.writerow(['project', project, ','.join(spacetokens), space, used])

            for k, used in sorted(pshf.items()):
                project, spacetoken, host, fs = k
                if spacetoken not in spacetokencfg: continue # unknown spacetoken
                sproject, spath, sgroups, sspace, shostfss = spacetokencfg[spacetoken]
                expected = 0
                if (host, fs) in shostfss:
                    total = 0
                    for shost, sfs in shostfss:
                        total += self._get_fs_space(shost, sfs)[0]
                    expected = int(0 if total == 0 else float(sspace * self._get_fs_space(host, fs)[0]) / total)
                writer.writerow(['used', project, spacetoken, host, fs, used, expected])

        _log.debug("migration used groupuser: %i", len(used_groupuser))
        _log.debug("migration used spacetokens: %i", len(used_spacetokens))
        _log.debug("migration used pools: %i", len(used_pools))
        _log.debug("migration used hosts: %i", len(used_hosts))
        _log.debug("migration used fss: %i", len(used_fss))

        end_time_cpu = time.process_time()
        end_time_mono = time.monotonic()

        _log.info("export of %i records completed in %.01fs (cpu %.01fs, eff %.01f%%): dirs %i, files %i (replicas %i), links %i, unknown %i",
                  cnt.get('ALL', 0), end_time_mono - start_time_mono, end_time_cpu - start_time_cpu,
                  0. if end_time_mono == start_time_mono else 100. * (end_time_cpu - start_time_cpu) / (end_time_mono - start_time_mono),
                  cnt.get('DIR', 0), cnt.get('FILE', 0), cnt.get('REPLICA', 0),
                  cnt.get('LINK', 0), cnt.get('UNKNOWN', 0))


#=====================================================================
# import dumped data into dCache database
#=====================================================================

class dcache(object):
    # based mostly on dCache chimera sources
    # modules/chimera/src/main/java/org/dcache/chimera/FsSqlDriver.java (create object, path2inode)
    # modules/chimera/src/main/java/org/dcache/chimera/JdbcFs.java (links, files, dirs, ignore copyAcl, copyTags)

    # dCache constants
    UnixPermission_S_PERMS = 0o7777
    FileState_CREATED = 1
    StorageGenericLocation_DISK = 1
    RsType_DIR = 0x00000000
    RsType_FILE = 0x00000001
    AccessLatency_NEARLINE = 0
    AccessLatency_ONLINE = 1
    RetentionPolicy_CUSTODIAL = 0
    RetentionPolicy_OUTPUT = 1
    RetentionPolicy_REPLICA = 2
    AceType_ACCESS_ALLOWED_ACE_TYPE = 0x00000000
    AceType_ACCESS_DENIED_ACE_TYPE = 0x00000001
    AceType_ACCESS_AUDIT_ACE_TYPE = 0x00000002
    AceType_ACCESS_ALARM_ACE_TYPE = 0x00000003
    AccessMask_READ_DATA = 0x00000001
    AccessMask_LIST_DIRECTORY = 0x00000001
    AccessMask_WRITE_DATA = 0x00000002
    AccessMask_ADD_FILE = 0x00000002
    AccessMask_APPEND_DATA = 0x00000004
    AccessMask_ADD_SUBDIRECTORY = 0x00000004
    AccessMask_READ_NAMED_ATTRS = 0x00000008
    AccessMask_WRITE_NAMED_ATTRS = 0x00000010
    AccessMask_EXECUTE = 0x00000020
    AccessMask_DELETE_CHILD = 0x00000040
    AccessMask_READ_ATTRIBUTES = 0x00000080
    AccessMask_WRITE_ATTRIBUTES = 0x00000100
    AccessMask_DELETE = 0x00010000
    AccessMask_READ_ACL = 0x00020000
    AccessMask_WRITE_ACL = 0x00040000
    AccessMask_WRITE_OWNER = 0x00080000
    AccessMask_SYNCHRONIZE = 0x00100000
    AceFlags_FILE_INHERIT_ACE = 0x00000001
    AceFlags_DIRECTORY_INHERIT_ACE = 0x00000002
    AceFlags_INHERIT_ONLY_ACE_LEGACY = 0x00000008
    AceFlags_INHERIT_ONLY_ACE = 0x00000008
    AceFlags_IDENTIFIER_GROUP = 0x00000040
    Who_USER = 0x00000000
    Who_GROUP = 0x00000001
    Who_OWNER = 0x00000002
    Who_OWNER_GROUP = 0x00000003
    Who_EVERYONE = 0x00000004
    Who_ANONYMOUS = 0x00000005
    Who_AUTHENTICATED = 0x00000006
    SpaceState_RESERVED = 0
    SpaceState_RELEASED = 1
    SpaceState_EXPIRED = 2

    ChecksumType = { # ChecksumType
        'adler32': 1,
        'md5': 2,
        'md4': 3,
        'sha1': 4,
        'sha256': 5,
        'sha512': 6,
    }

    def __init__(self, conn_chimera=None, conn_spacemanager=None, src_path='/', dst_path='/'):
        """Import directory+file+link data in dCache namespace
        parameters:
          conn - postgresql connection
          src_path - import only directories within given source path (default: /)
          dst_path - dcache base path for import, directory must exist before import (default: /)
        """
        self._src_path = src_path

        self._commit_size = 1000 # explicit commit after 1000 records when autocommit is disabled

        if conn_chimera != None:
            self._conn = conn_chimera
            self._conn.autocommit = False
            self._cursor = self._conn.cursor()
            self._cursor.execute("SET synchronous_commit = OFF")
            #self._cursor.execute("SET wal_writer_delay = 10000")
            #self._cursor.execute("SET wal_writer_flush_after = 0")

            self._rootid = self._inumber('000000000000000000000000000000000000')
            if dst_path != '/':
                for fragment in dst_path.lstrip('/').split('/'):
                    self._rootid = self._inodeOf(self._rootid, fragment)

        if conn_spacemanager != None:
            self._conn_spacemanager = conn_spacemanager
            self._conn_spacemanager.autocommit = False
            self._cursor_spacemanager = self._conn_spacemanager.cursor()
            self._cursor_spacemanager.execute("SET synchronous_commit = OFF")
            #self._cursor_spacemanager.execute("SET wal_writer_delay = 10000")
            #self._cursor_spacemanager.execute("SET wal_writer_flush_after = 0")

        self.reset_config()


    def reset_config(self, **kw):
        # uid and gid used for unmapped users/groups
        self._uid = kw.get('uid-first', 10000)
        self._gid = kw.get('gid-first', 1000)
        self._vo_inc = kw.get('vo-gid-inc', 1000)
        self._user2uid = {'root': 0}
        self._group2gid = {'root': 0}
        self._user2nuser = {}
        self._group2ngroup = {}
        self._group2uid = {}
        self._cached_ace_list = {}
        self._warn_once_set = {}
        self._config = {
            'headnode': socket.getfqdn(),
            'user': {},
            'group': {},
            'group2uid': {},
            'project': {},
            'filesystem': {},
            'spacetoken': {},
            'used': [],
            'path': [],
            'comment': [],
        }

        self._p2i = []
        self._p2i_last_path = None
        self._p2i_last_inumber = None

        self._domain_pools = {}
        self._hostfs2pool = {}

        self._tag2id = {}

        self._spacetoken2id = None


    def _warn_once(self, message, wid=None, cnt=1):
        cnt_logged = self._warn_once_set.get(message if wid == None else wid, 0)
        if cnt_logged < cnt:
            _log.warn(message)
        self._warn_once_set[message if wid == None else wid] = cnt_logged + 1


    def _next_uid(self, nuser):
        # gid number can be used as uid for unmapped users
        # skip not just uids but als existing gid numbers
        # NOTE: we could also skip all allocated VO gid ranges
        while self._uid in self._user2uid.values() or self._uid in self._group2gid.values() or self._uid in self._group2uid.values():
            self._uid += 1
        return self._uid


    def _add_uid(self, user, cuser, uid=None):
        nuser = self._dn2user(user, cuser)
        if nuser not in self._user2uid:
            if uid == None:
                uid = self._next_uid(nuser)

            self._user2uid[nuser] = uid

        else:
            if uid == None:
                uid = self._user2uid[nuser]

        if uid != self._user2uid[nuser]:
            _log.warn("replace config user %s %s with %s", user, self._config['user'][user], (nuser, uid))

        # add new user to the configuration
        self._config['user'][user] = (nuser, uid)


    def _get_or_create_uid(self, user, group):
        nuser = self._dn2user(user)
        if nuser not in self._user2uid:
            if group == None:
                uid = self._next_uid(nuser)
                self._user2uid[nuser] = uid
                # add new user to the configuration
                self._config['user'][user] = (nuser, uid)
            else:
                # return primary group id for unmapped users
                gid = self._get_or_create_gid(group)
                ngroup = self._fqan2group(group)
                return self._group2uid.get(ngroup, (ngroup, gid))[1]

        return self._user2uid[nuser]


    def _get_mapped_uid(self, user, group=None, default=None, map_mode=['create']):
        uid = None

        for m in map_mode:
            if m == 'create':
                uid = self._get_or_create_uid(user, group)
                break
            elif m == 'original' and user in self._user2nuser and self._user2nuser[user] in self._user2uid:
                uid = self._user2uid[self._user2nuser[user]]
                break
            elif m == 'normalized' and user in self._user2uid:
                uid = self._user2uid[user]
                break

        return uid if uid != None else default


    def _next_gid(self, ngroup):
        # try to have gid numbers in increasing
        # subsequent order for all groups from one VO
        ngroup_base = ngroup.split('_')[0]
        if ngroup_base not in self._group2gid:
            while self._gid in self._group2gid.values():
                self._gid = ((self._gid + self._vo_inc) // self._vo_inc) * self._vo_inc
            return self._gid

        gid = self._group2gid[ngroup_base]
        while gid in self._group2gid.values():
            gid += 1
        return gid


    def _add_gid(self, group, cgroup, gid=None):
        ngroup = self._fqan2group(group, cgroup)
        if ngroup not in self._group2gid:
            if gid == None:
                gid = self._next_gid(ngroup)

            self._group2gid[ngroup] = gid

        else:
            if gid == None:
                gid = self._group2gid[ngroup]

        if gid != self._group2gid[ngroup]:
            _log.warn("replace config group %s %s with %s", group, self._config['group'][group], (ngroup, gid))

        # add new group to the configuration
        self._config['group'][group] = (ngroup, gid)


    def _get_or_create_gid(self, group):
        ngroup = self._fqan2group(group)
        if ngroup not in self._group2gid:
            gid = self._next_gid(ngroup)
            self._group2gid[ngroup] = gid
            # add new group to the configuration
            self._config['group'][group] = (ngroup, gid)

        return self._group2gid[ngroup]


    def _get_mapped_gid(self, group, default=None, map_mode=['create']):
        gid = None

        for m in map_mode:
            if m == 'create':
                gid = self._get_or_create_gid(group)
                break
            elif m == 'original' and group in self._group2ngroup and self._group2ngroup[group] in self._group2gid:
                gid = self._group2gid[self._group2ngroup[group]]
                break
            elif m == 'normalized' and group in self._group2gid:
                gid = self._group2gid[group]
                break

        return gid if gid != None else default


    def _get_dpool(self, host, fs, project):
        if (host, fs, project) not in self._hostfs2pool:
            dpcnt = self._domain_pools.get(project, 0) + 1
            self._domain_pools[project] = dpcnt
            pool_name = "{0}_{1:03d}".format(project, dpcnt)
            self._hostfs2pool[(host, fs, project)] = pool_name

        return self._hostfs2pool[(host, fs, project)]


    def _get_dpool_nordugrid(self, host, fs):
        if (host, fs) not in self._hostfs2pool:
            #domain_name = host.replace('.', '_')
            domain = host[host.find('.')+1:].replace('.', '_').lower()
            dpcnt = self._domain_pools.get(domain, 0) + 1
            self._domain_pools[domain] = dpcnt
            pool_name = "{0}_{1:03d}".format(domain, dpcnt)
            self._hostfs2pool[(host, fs)] = pool_name

        return self._hostfs2pool[(host, fs)]


    def _dn2user(self, dn, cuser=None):
        if dn in self._user2nuser:
            if cuser != None and self._user2nuser[dn] != cuser:
                _log.warn("overwriting user %s mapping from %s to %s", dn, self._user2nuser[dn], cuser)
                self._user2nuser[dn] = cuser

        else:
            if dn == 'root':
                username = 'root'

            #elif dn.startswith('group:'):
            #    username = dn[len('group:'):]

            else:
                cns = [x for x in dn.split('/CN=') if not x.startswith('/')]
                if len(cns) == 0:
                    cn = 'user'
                else:
                    # lowercase content of first CN with replaced special characters using '_'
                    cn = ''.join([x if x >= 'a' and x <= 'z' else '_' for x in cns[0].lower()])

                # create unique username
                cnt = 1
                username = cn
                while username == 'user' or username in self._user2nuser.values():
                    username = "{0}_{1}".format(cn, cnt)
                    cnt += 1

            self._user2nuser[dn] = username

        return self._user2nuser[dn]


    def _fqan2group(self, fqan, cgroup=None):
        if fqan in self._group2ngroup:
            if cgroup != None and self._group2ngroup[fqan] != cgroup:
                _log.warn("overwriting group %s mapping from %s to %s", fqan, self._group2ngroup[fqan], cgroup)
                self._group2ngroup[fqan] = cgroup

        else:
            if cgroup != None:
                self._group2ngroup[fqan] = cgroup
            else:
                tmp = fqan
                tmp = tmp.lstrip('/')
                tmp = tmp.replace('/Role=', '/')
                tmp = tmp.replace('/Capability=', '/')
                tmp = tmp.replace('/', '_')
                tmp = tmp.lower()
                self._group2ngroup[fqan] = tmp

        return self._group2ngroup[fqan]


    def _build_ace_list(self, path, isdir, acl, map_mode=['create']):
        if acl.strip() == '':
            return []

        if acl in self._cached_ace_list:
            return self._cached_ace_list[acl]

        # ace_type, flags, access_msk, who, who_id = ace
        ace_list = []
        for atype, amode, aname in json.loads(acl):
            flags = 0
            if atype.startswith('default_'):
                flags |= dcache.AceFlags_FILE_INHERIT_ACE
                flags |= dcache.AceFlags_DIRECTORY_INHERIT_ACE

            access_msk = 0
            if isdir:
                if amode[0] == 'r':
                    access_msk |= dcache.AccessMask_LIST_DIRECTORY     # l
                if amode[1] == 'w':
                    access_msk |= dcache.AccessMask_ADD_FILE           # f
                    access_msk |= dcache.AccessMask_ADD_SUBDIRECTORY   # s
                    access_msk |= dcache.AccessMask_DELETE             # d
                    access_msk |= dcache.AccessMask_DELETE_CHILD       # D
                if amode[2] == 'x':
                    access_msk |= dcache.AccessMask_EXECUTE            # x
            else:
                if amode[0] == 'r':
                    access_msk |= dcache.AccessMask_READ_DATA          # r
                if amode[1] == 'w':
                    access_msk |= dcache.AccessMask_WRITE_DATA         # w
                if amode[2] == 'x':
                    access_msk |= dcache.AccessMask_EXECUTE            # x

            if atype in ['user', 'default_user']:
                # ignore ACL for users not defined / removed from config
                who = dcache.Who_USER
                who_id = 0
                if aname != '':
                    who_id = self._get_mapped_uid(aname, map_mode=map_mode)
                else:
                    who = dcache.Who_OWNER
                if who_id != None:
                    ace_list.append((dcache.AceType_ACCESS_ALLOWED_ACE_TYPE, flags, access_msk, who, who_id))
                else:
                    self._warn_once("ignoring {0} ACL for unmapped user {1} (map mode: {2})".format(path, aname, ', '.join(map_mode)), wid=aname)
            elif atype in ['group', 'default_group']:
                who = dcache.Who_GROUP
                who_id = 0
                if aname != '':
                    who_id = self._get_mapped_gid(aname, map_mode=map_mode)
                else:
                    who = dcache.Who_OWNER_GROUP
                if who_id != None:
                    ace_list.append((dcache.AceType_ACCESS_ALLOWED_ACE_TYPE, flags, access_msk, who, who_id))
                else:
                    self._warn_once("ignoring {0} ACL for unmapped group {1} (map mode: {2})".format(path, aname, ', '.join(map_mode)), wid=aname)
            elif atype in ['other', 'default_other']:
                who = dcache.Who_EVERYONE
                who_id = 0
                ace_list.append((dcache.AceType_ACCESS_ALLOWED_ACE_TYPE, flags, access_msk, who, who_id))
            else:
                _log.warn("Unsupported %s ACL type \"%s\"", path, atype)

        if len(self._cached_ace_list) > 10000:
            self._cached_ace_list.clear()
        self._cached_ace_list[acl] = ace_list

        return ace_list


    def _get_spacetoken_id(self, spacetoken):
        if spacetoken == None:
            return None

        if self._spacetoken2id == None:
            self._spacetoken2id = {}
            #cursor = self._conn.cursor()
            cursor = self._cursor_spacemanager
            try:
                sql = "SELECT id, description FROM srmspace"
                cursor.execute(sql)
                for tokenid, description in cursor:
                    self._spacetoken2id[description] = tokenid
                _log.debug("spacetokens: %s", self._spacetoken2id)
            finally:
                #cursor.close()
                pass

        return self._spacetoken2id.get(spacetoken)


    def _set_spacetoken_size(self, spacetoken, size):
        #cursor = self._conn.cursor()
        cursor = self._cursor_spacemanager
        try:
            sql = "SELECT id, linkgroupid, sizeinbytes FROM srmspace WHERE description = %s ORDER BY id ASC"
            cursor.execute(sql, (spacetoken, ))
            if cursor.rowcount == 0:
                _log.warn("Spacetoken %s not defined in dCache (corresponding files & directories can't be associated with this spacetoken)", spacetoken)
                return
            tokenid, linkgroupid, sizeinbytes = cursor.fetchone()
            if cursor.rowcount > 1:
                _log.warn("More spacetokens defined for %s, using first one with tokenid %s", spacetoken, tokenid)
            if size == sizeinbytes:
                return
            _log.info("update spacetoken %s and corresponding linkgroup size by %s", spacetoken, size - sizeinbytes)
            sql = "UPDATE srmlinkgroup SET availablespaceinbytes = availablespaceinbytes + %s WHERE id = %s"
            cursor.execute(sql, (size - sizeinbytes, linkgroupid))
            sql = "UPDATE srmspace SET sizeinbytes = sizeinbytes + %s WHERE description = %s"
            cursor.execute(sql, (size - sizeinbytes, spacetoken))
        finally:
            #cursor.close()
            pass


    # create new dCache ipnfsid
    @staticmethod
    def _newID(path, fsId=0):
        return "{0:04X}{1}".format((fsId >> 32) & 0xffff, uuid.uuid5(uuid.NAMESPACE_URL, path).hex.upper())


    def _inumber(self, pnfsid):
        #cursor = self._conn.cursor()
        cursor = self._cursor
        try:
            sql = "SELECT inumber FROM t_inodes WHERE ipnfsid = %s"
            cursor.execute(sql, (pnfsid, ))
            return cursor.fetchone()[0]
        finally:
            #cursor.close()
            pass


    def _inodeOf(self, parentid, name):
        #cursor = self._conn.cursor()
        cursor = self._cursor
        try:
            sql = "SELECT c.inumber FROM t_dirs d JOIN t_inodes c ON d.ichild = c.inumber WHERE d.iparent = %s AND d.iname = %s"
            cursor.execute(sql, (parentid, name))
            return cursor.fetchone()[0]
        finally:
            #cursor.close()
            pass


    def _path2inode_add(self, path, inumber):
        #_log.debug("path2inode_add(%s, %i)", path, inumber)

        self._p2i.append((path, inumber))
        self._p2i_last_path = path
        self._p2i_last_inumber = inumber

    def _path2inode_get(self, path):
        #_log.debug("path2inode_get(%s)", path)

        if path == self._p2i_last_path:
            return self._p2i_last_inumber

        while True:
            if len(self._p2i) == 0:
                raise Exception("invalid order of filesystem objects, unable to resolve inumber for path {0}".format(path))

            old_path, old_inumber = self._p2i.pop()
            if old_path == path:
                self._p2i_last_path = old_path
                self._p2i_last_inumber = old_inumber
                self._path2inode_add(old_path, old_inumber)
                return old_inumber


    def _createInodeInParent(self, cursor, pnfsid, parentid, name, uid, gid, mode, ctime, atime, mtime, ftype, size=0):
        # createInode
        nlink = 2 if (ftype & stat.S_IFDIR) == stat.S_IFDIR else 1

        sql = "INSERT INTO t_inodes (ipnfsid,itype,imode,inlink,iuid,igid,isize,iio,ictime,iatime,imtime,icrtime,igeneration,iaccess_latency,iretention_policy) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,to_timestamp(%s)::date,to_timestamp(%s)::date,to_timestamp(%s)::date,to_timestamp(%s)::date,%s,%s,%s) RETURNING inumber"
        cursor.execute(sql, (pnfsid, ftype, mode & dcache.UnixPermission_S_PERMS, nlink, uid, gid, size, dcache.FileState_CREATED, ctime, atime, mtime, ctime, 1, dcache.AccessLatency_ONLINE, dcache.RetentionPolicy_REPLICA))
        inumber = cursor.fetchone()[0]

        # createEntryInParent
        sql = "INSERT INTO t_dirs (iparent,ichild,iname) VALUES(%s,%s,%s)"
        cursor.execute(sql, (parentid, inumber, name))

        # incNlink for parentid
        sql = "UPDATE t_inodes SET inlink=inlink+1,igeneration=igeneration+1 WHERE inumber=%s"
        cursor.execute(sql, (parentid, ))

        return inumber


    def _checksum(self, inumber, ctype, value):
        if ctype not in dcache.ChecksumType:
            return

        #cursor = self._conn.cursor()
        cursor = self._cursor
        try:
            # setInodeChecksum
            sql = "INSERT INTO t_inodes_checksum (inumber,itype,isum) VALUES (%s,%s,%s)"
            cursor.execute(sql, (inumber, dcache.ChecksumType[ctype], value))
        finally:
            #cursor.close()
            pass


    def _writeAcl(self, inumber, rs_type, acl):
        if len(acl) == 0:
            return

        #cursor = self._conn.cursor()
        cursor = self._cursor
        try:
            for order, ace in enumerate(acl):
                ace_type, flags, access_msk, who, who_id = ace
                sql = "INSERT INTO t_acl (inumber,rs_type,type,flags,access_msk,who,who_id,ace_order) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"
                cursor.execute(sql, (inumber, rs_type, ace_type, flags, access_msk, who, who_id, order))
        finally:
            #cursor.close()
            pass


    def _createTag(self, inumber, name, data, mode):
        cursor = self._cursor
        try:
            mode |= stat.S_IFREG
            isorign = 0
            if (name, data, mode) not in self._tag2id:
                sql = "INSERT INTO t_tags_inodes (imode, inlink, iuid, igid, isize, ictime, iatime, imtime, ivalue) VALUES (%s,0,%s,%s,%s,NOW(),NOW(),NOW(),%s) RETURNING itagid"
                cursor.execute(sql, (mode, 0, 0, len(data), data))
                isorign = 1
                self._tag2id[(name, data, mode)] = cursor.fetchone()[0]
            itagid = self._tag2id[(name, data, mode)]

            sql = "INSERT INTO t_tags (inumber, itagid, isorign, itagname) VALUES(%s,%s,%s,%s)"
            cursor.execute(sql, (inumber, itagid, isorign, name))

            sql = "UPDATE t_tags_inodes SET inlink = inlink + 1 WHERE itagid=%s"
            cursor.execute(sql, (itagid, ))
        finally:
            #cursor.close()
            pass


    def _insertFile(self, reservationId, voGroup, voRole, sizeInBytes, pnfsId, state):
        cursor = self._cursor_spacemanager
        try:
            sql = "INSERT INTO srmspacefile (vogroup, vorole, spacereservationid, sizeinbytes, creationtime, pnfsid, state) VALUES (%s,%s,%s,%s,(EXTRACT(EPOCH FROM NOW()) * 1000)::bigint,%s,%s)"
            cursor.execute(sql, (voGroup, voRole, reservationId, sizeInBytes, pnfsId, state))
        finally:
            #cursor.close()
            pass


    def replica(self, inumber, location, ctime, atime):
        #cursor = self._conn.cursor()
        cursor = self._cursor
        try:
            # addInodeLocation
            itype = dcache.StorageGenericLocation_DISK
            priority = 10 # default priority
            state = 1 # online
            sql = "INSERT INTO t_locationinfo (inumber,itype,ilocation,ipriority,ictime,iatime,istate) VALUES (%s,%s,%s,%s,to_timestamp(%s)::date,to_timestamp(%s)::date,%s)"
            cursor.execute(sql, (inumber, itype, location, priority, ctime, atime, state))
        finally:
            #cursor.close()
            pass


    def file(self, path, uid, gid, mode, acllist, atime, ctime, mtime, size, spacetoken, csums):
        if path == '/':
            raise Exception("root directory can't be a file")

        parentpath, name = path.rsplit('/', 1)
        parentid = self._path2inode_get(parentpath)

        #cursor = self._conn.cursor()
        cursor = self._cursor
        try:
            pnfsid = dcache._newID(path)
            inumber = self._createInodeInParent(cursor, pnfsid, parentid, name, uid, gid, mode, ctime, atime, mtime, stat.S_IFREG, size)
            self._writeAcl(inumber, dcache.RsType_FILE, acllist)
            for csumtype, csumvalue in csums.items():
                self._checksum(inumber, csumtype, csumvalue)
            if spacetoken not in [None, '']:
                tokenid = self._get_spacetoken_id(spacetoken)
                if tokenid != None:
                    self._insertFile(tokenid, None, None, size, pnfsid, dcache.SpaceState_EXPIRED)
            #self._conn.commit()

            return (inumber, pnfsid)
        finally:
            #cursor.close()
            pass


    def link(self, path, uid, gid, mode, acllist, atime, ctime, mtime, target):
        if path == '/':
            raise Exception("root directory can't be a link")

        parentpath, name = path.rsplit('/', 1)
        parentid = self._path2inode_get(parentpath)

        #cursor = self._conn.cursor()
        cursor = self._cursor
        try:
            data = target.encode('utf-8')
            pnfsid = dcache._newID(path)
            inumber = self._createInodeInParent(cursor, pnfsid, parentid, name, uid, gid, mode, ctime, atime, mtime, stat.S_IFLNK, len(data))
            self._writeAcl(inumber, dcache.RsType_FILE, acllist)
            sql = "INSERT INTO t_inodes_data (inumber,ifiledata) VALUES (%s,%s)"
            cursor.execute(sql, (inumber, data))
            #self._conn.commit()
            #return inumber
        finally:
            #cursor.close()
            pass


    def dir(self, path, uid, gid, mode, acllist, atime, ctime, mtime, spacetoken):
        #if path == '/':
        #    raise Exception("root directory should already exist")
        if path == '':
            self._path2inode_add(path, self._rootid)
            return

        parentpath, name = path.rsplit('/', 1)
        parentid = self._path2inode_get(parentpath)

        #cursor = self._conn.cursor()
        cursor = self._cursor
        try:
            pnfsid = dcache._newID(path)
            inumber = self._createInodeInParent(cursor, pnfsid, parentid, name, uid, gid, mode, ctime, atime, mtime, stat.S_IFDIR, 512)
            self._writeAcl(inumber, dcache.RsType_DIR, acllist)
            if spacetoken not in [None, '']:
                tokenid = self._get_spacetoken_id(spacetoken)
                if tokenid != None:
                    self._createTag(inumber, 'WriteToken', "{0}".format(tokenid), mode & 0o666)
            #self._conn.commit()
            self._path2inode_add(path, inumber)
            #return inumber
        finally:
            #cursor.close()
            pass


    def import_csv(self, nsfile, skip_acl, skip_writetoken):
        _log.info("import data from %s to dCache database (skip_acl=%s, skip_writetoken=%s)", nsfile, skip_acl, skip_writetoken)

        if len(self._config['spacetoken']) == 0:
            raise Exception("Importing dCache namespace without any spacetoken is not currently supported")

        # spacetoken to project mapping from real info
        # about used space regardless of mismatched filesystem
        spacetoken2project = {}
        for spacetoken in self._config['spacetoken'].keys():
            if spacetoken.find('"') != -1:
                raise Exception("Unsupported character in the spacetoken description: %s" % spacetoken)
            project, path, groups, space, used = self._config['spacetoken'][spacetoken]
            spacetoken2project[spacetoken] = project
            self._set_spacetoken_size(spacetoken, space)

        start_time_mono = time.monotonic()
        start_time_cpu = time.process_time()

        record_time_mono = start_time_mono
        record_time_cpu = start_time_cpu

        host2link_csvfile = {}
        host2link_writer = {}
        with io.open(nsfile, newline='') as csvfile:
            reader = csv.reader(csvfile, quotechar="'", quoting=csv.QUOTE_MINIMAL)
            lastpath = None
            lastinumber = None
            cnt = {}
            for row in reader:
                tobj = row[0]
                cnt['ALL'] = cnt.get('ALL', 0) + 1

                tobj, path, user, group, mode, acl, atime, ctime, mtime, spacetoken = row[:10]

                # overwrite existing permission with user defined values
                # for most specific directory from config file
                map_mode = ['create']
                path_config_last = None
                for path_config in self._config['path']:
                    tpath, tuser, tgroup, tmode, tacl, tspacetoken = path_config
                    if path.startswith(tpath) and (path_config_last == None or len(tpath) > len(path_config_last[0])):
                        path_config_last = path_config
                if path_config_last != None:
                    if _log.getEffectiveLevel() < logging.DEBUG:
                        _log.debug("path %s use attributes explicitly configured for %s", path, path_config_last)
                    tpath, tuser, tgroup, tmode, tacl, tspacetoken = path_config_last
                    if tuser != '': user = tuser
                    if tgroup != '': group = tgroup
                    if tmode != '': mode = tmode
                    if tacl != '':
                        acl = tacl
                        map_mode = ['normalized', 'original']
                    if tspacetoken != '': spacetoken = tspacetoken

                uid = self._get_or_create_uid(user, group)
                gid = self._get_or_create_gid(group)
                acllist = [] if skip_acl else self._build_ace_list(path, tobj == 'DIR', acl, map_mode=map_mode)

                # create directory/file/link object in dCache namespace
                if tobj == 'DIR':
                    dirspacetoken = None if skip_writetoken else spacetoken
                    self.dir(path, uid, gid, int(mode, 8), acllist, int(atime), int(ctime), int(mtime), dirspacetoken)
                    cnt[tobj] = cnt.get(tobj, 0) + 1

                elif tobj == 'LINK':
                    target = row[10]
                    self.link(path, uid, gid, int(mode, 8), acllist, int(atime), int(ctime), int(mtime), target)
                    cnt[tobj] = cnt.get(tobj, 0) + 1

                elif tobj == 'FILE':
                    size, csums, pool, host, fs, sfn = row[10:]
                    project = spacetoken2project.get(spacetoken)

                    if spacetoken == '':
                        _log.warn("file with empty spacetoken: %s", path)
                    elif spacetoken not in spacetoken2project:
                        _log.warn("file with spacetoken %s not mapped to any project: %s", spacetoken, path)

                    if project == None and (host, fs) in self._config['filesystem']:
                        # try to find right project for host filesystem
                        hostfs_projects, hostfs_spacetokens, hostfs_space, hostfs_used = self._config['filesystem'][(host, fs)]
                        if len(hostfs_projects) == 1:
                            project = hostfs_projects[0]
                        else:
                            # find project with biggest space and use it for data
                            # that were not associated with any spacetoken
                            max_project_space = 0
                            for project_project in self._config['project'].keys():
                                if project_project not in hostfs_projects: continue
                                project_spacetokens, project_space, project_used = self._config['project']
                                if project_space > max_project_space:
                                    project = project_project
                                    max_project_space = project_space

                    if project == None:
                        _log.error("ignoring file that can't be associated with any project: %s (spacetoken %s)", path, spacetoken)
                        cnt['UNKNOWN'] = cnt.get('UNKNOWN', 0) + 1

                    else:
                        dpool = self._get_dpool(host, fs, project)

                        if path != lastpath:
                            filespacetoken = None if skip_writetoken else spacetoken
                            lastinumber, pnfsid = self.file(path, uid, gid, int(mode, 8), acllist, int(atime), int(ctime), int(mtime), int(size), filespacetoken, json.loads(csums))
                            lastpath = path
                            writer = host2link_writer.get(host)
                            if writer == None:
                                csvfile = io.open("move-{0}.csv".format(host), 'w', newline='')
                                writer = csv.writer(csvfile, quotechar="'", quoting=csv.QUOTE_MINIMAL)
                                host2link_csvfile[host] = csvfile
                                host2link_writer[host] = writer
                                # always add all directories evethough they may be empty (no files)
                                hfs_projects = set()
                                for hfs_project, hfs_spacetoken, hfs_host, hfs_fs, hfs_used, hfs_expected in self._config['used']:
                                    if host != hfs_host: continue
                                    if (hfs_host, hfs_fs, hfs_project) in hfs_projects: continue
                                    hfs_projects.add((hfs_host, hfs_fs, hfs_project))
                                    writer.writerow([hfs_fs, '', "dcache/{0}/data".format(self._get_dpool(hfs_host, hfs_fs, hfs_project))])
                            # base path + relative source filename + relative destination filename
                            writer.writerow([fs, sfn, "dcache/{0}/data/{1}".format(dpool, pnfsid)])
                            cnt['FILE'] = cnt.get(tobj, 0) + 1

                        self.replica(lastinumber, dpool, int(atime), int(ctime))

                        cnt['REPLICA'] = cnt.get('REPLICA', 0) + 1

                else:
                    _log.error("unsupported / invalid file object %s for %s", tobj, path)
                    cnt['UNKNOWN'] = cnt.get('UNKNOWN', 0) + 1

                # with autocommit false use explicit commit every 1000 records
                if cnt['ALL'] % self._commit_size == 0:
                    self._conn.commit()
                    self._conn_spacemanager.commit()

                if cnt['ALL'] % 100000 == 0:
                    record_time_mono_new = time.monotonic()
                    record_time_cpu_new = time.process_time()
                    _log.info("processed %i records in %.01fs (cpu %.01fs, eff %.01f%%): dirs %i, files %i (replicas %i), links %i, unknown %i",
                              cnt.get('ALL', 0), record_time_mono_new - record_time_mono, record_time_cpu_new - record_time_cpu,
                              0. if record_time_mono_new == record_time_mono else 100. * (record_time_cpu_new - record_time_cpu) / (record_time_mono_new - record_time_mono),
                              cnt.get('DIR', 0), cnt.get('FILE', 0), cnt.get('REPLICA', 0), cnt.get('LINK', 0), cnt.get('UNKNOWN', 0))
                    record_time_mono = record_time_mono_new
                    record_time_cpu = record_time_cpu_new

        self._conn.commit()
        self._conn_spacemanager.commit()

        for f in host2link_csvfile.values():
            f.close()

        end_time_cpu = time.process_time()
        end_time_mono = time.monotonic()

        _log.info("import of %i records completed in %.01fs (cpu %.01fs, eff %.01f%%): dirs %i, files %i (replicas %i), links %i, unknown %i",
                  cnt.get('ALL', 0), end_time_mono - start_time_mono, end_time_cpu - start_time_cpu,
                  0. if end_time_mono == start_time_mono else 100. * (end_time_cpu - start_time_cpu) / (end_time_mono - start_time_mono),
                  cnt.get('DIR', 0), cnt.get('FILE', 0),
                  cnt.get('REPLICA', 0), cnt.get('LINK', 0), cnt.get('UNKNOWN', 0))


    def read_config(self, configfile):
        user_tmp = {}
        group2uid_tmp = {}
        path_tmp = []

        with io.open(configfile, newline='') as csvfile:
            reader = csv.reader(csvfile, quotechar="'", quoting=csv.QUOTE_MINIMAL)
            for row in reader:
                if row[0] == 'headnode':
                    self._config['headnode'] = row[1]
                elif row[0] == 'user':
                    orig_user = row[1]
                    user = row[2] if len(row) > 2 else self._dn2user(orig_user)
                    uid = int(row[3]) if len(row) > 3 else None
                    user_tmp[orig_user] = (user, uid)
                elif row[0] == 'group':
                    orig_group = row[1]
                    group = row[2] if len(row) > 2 else self._fqan2group(orig_group)
                    gid = int(row[3]) if len(row) > 3 else None
                    self._add_gid(orig_group, group, gid)
                elif row[0] == 'group2uid':
                    group = row[1]
                    user = row[2] if len(row) > 2 else group
                    uid = int(row[3]) if len(row) > 3 else None
                    group2uid_tmp[group] = (user, uid)
                elif row[0] == 'filesystem':
                    projects = [x for x in row[1].split(',') if x.strip() != '']
                    spacetokens = [x for x in row[2].split(',') if x.strip() != '']
                    host = row[3]
                    fs = row[4]
                    space = int(row[5])
                    used = int(row[6])
                    self._config['filesystem'][(host, fs)] = (projects, spacetokens, space, used)
                elif row[0] == 'spacetoken':
                    project = row[1]
                    spacetoken = row[2]
                    path = row[3]
                    groups = [] if row[4] == '' else row[4].split(',')
                    space = int(row[5])
                    used = int(row[6])
                    self._config['spacetoken'][spacetoken] = (project, path, groups, space, used)
                elif row[0] == 'project':
                    project = row[1]
                    spacetokens = [x for x in row[2].split(',') if x.strip() != '']
                    space = int(row[3])
                    used = int(row[4])
                    self._config['project'][project] = (spacetokens, space, used)
                elif row[0] == 'used':
                    project = row[1]
                    spacetoken = row[2]
                    host = row[3]
                    fs = row[4]
                    used = int(row[5])
                    expected = int(row[6])
                    self._config['used'].append((project, spacetoken, host, fs, used, expected))
                elif row[0] == 'path':
                    path = row[1]
                    user = row[2] if len(row) > 2 else ''
                    group = row[3] if len(row) > 3 else ''
                    mode = row[4] if len(row) > 4 else ''
                    acl = row[5] if len(row) > 5 else ''
                    spacetoken = row[6] if len(row) > 6 else ''
                    path_tmp.append((path, user, group, mode, acl, spacetoken))
                elif len(row[0].strip()) > 0 and row[0].strip()[0] == '#':
                    self._config['comment'].append(row)
                else:
                    _log.warn("unknown config data: %s", row[0])

            if self._uid == -1: # replace default first uid
                self._uid = max(((max(self._group2gid.values()) + 5*self._vo_inc) // self._vo_inc) * self._vo_inc, 10000)

            # first add all groups before generating uids not to
            # allocate uid numbers for individual users that match
            # existing gid numbers that could be used later for
            # unmapped vo users
            for orig_user in user_tmp.keys():
                user, uid = user_tmp[orig_user]
                self._add_uid(orig_user, user, uid)
            # first we need all groups and only then we can verify
            # configured groups exists. This is currently used only
            # to produce logging, because without explicit mapping
            # we can always use directly gid number resolved later.
            for group, useruid in group2uid_tmp.items():
                user, uid = useruid
                if uid == None:
                    if group not in self._group2gid:
                        if group not in self._group2ngroup:
                            _log.info("ignoring unknown group %s for group2uid configuration without explicit uid number", group)
                        else:
                            _log.warn("ignoring unknown group %s for group2uid configuration (normalized group name %s should be used in config file)", group, self._group2ngroup[group])
                        continue
                    uid = self._group2gid[group]
                self._group2uid[group] = (user, uid)
                self._config['group2uid'][group] = (user, uid)

            # verify if defined users/groups exists and log warnings
            # also test if defined ACL can be parsed
            for path, user, group, mode, acl, spacetoken in path_tmp:
                if user != '' and user not in self._user2uid:
                    # we can use primary group as a username
                    # and in such case missing user mapping can be OK
                    if group != '' and group not in self._group2gid:
                        details = "" if user not in self._user2nuser else " (normalized user {0} should be used in config file)".format(self._user2nuser[user])
                        _log.warn("no uid mapping for user %s configured for path %s%s", user, path, details)
                if group != '' and group not in self._group2gid:
                    details = "" if group not in self._group2ngroup else " (normalized user {0} should be used in config file)".format(self._group2ngroup[group])
                    _log.warn("no gid mapping for group %s configured for path %s%s", group, path, details)
                if acl.strip() != '':
                    for atype, amode, aname in json.loads(acl):
                        if atype in ['user', 'default_user']:
                            user = aname
                            if user not in self._user2uid:
                                details = "" if user not in self._user2nuser else " (normalized user {0} should be used in config file)".format(self._user2nuser[user])
                                _log.warn("no uid mapping for ACL user %s configured for path %s%s", user, path, details)
                        elif atype in ['group', 'default_group']:
                            group = aname
                            if group not in self._group2gid:
                                details = "" if group not in self._group2ngroup else " (normalized user {0} should be used in config file)".format(self._group2ngroup[group])
                                _log.warn("no gid mapping for ACL group %s configured for path %s%s", group, path, details)
                self._config['path'].append((path, user, group, mode, acl, spacetoken))

            # populate cache with poolnames
            for project, spacetoken, host, fs, used, expected in self._config['used']:
                self._get_dpool(host, fs, project)


    def write_config(self, configfile):
        with io.open(configfile, 'w', newline='') as f:
            writer = csv.writer(f, quotechar="'", quoting=csv.QUOTE_MINIMAL)

            writer.writerow(['headnode', self._config['headnode']])

            for orig_user, udata in sorted(self._config['user'].items()):
                user, uid = udata
                writer.writerow(['user', orig_user, user, uid])

            for orig_group, gdata in sorted(self._config['group'].items()):
                group, gid = gdata
                writer.writerow(['group', orig_group, group, gid])

            for group, useruid in sorted(self._config['group2uid'].items()):
                user, uid = useruid
                writer.writerow(['group2uid', group, user, uid])

            for filesystem, fdata in sorted(self._config['filesystem'].items()):
                host, fs = filesystem
                projects, spacetokens, space, used = fdata
                writer.writerow(['filesystem', ','.join(projects), ','.join(spacetokens), host, fs, space, used])

            for spacetoken, sdata in sorted(self._config['spacetoken'].items()):
                project, path, groups, space, used = sdata
                writer.writerow(['spacetoken', project, spacetoken, path, ','.join(groups), space, used])

            for project, pdata in sorted(self._config['project'].items()):
                spacetokens, space, used = pdata
                writer.writerow(['project', project, ','.join(spacetokens), space, used])

            for project, spacetoken, host, fs, used, expected in sorted(self._config['used']):
                writer.writerow(['used', project, spacetoken, host, fs, used, expected])

            for path, user, group, mode, acl, spacetoken in sorted(self._config['path']):
                writer.writerow(['path', path, user, group, mode, acl, spacetoken])

            for row in sorted(self._config['comment']):
                writer.writerow(row)


    def generate_config(self):
        with open("dcache.conf", "w") as fdcache, \
                open("gplazma.conf", "w") as fgplazma, \
                open("omnisession.conf", "w") as fomnisession, \
                open("ban.conf", "w") as fbanfile, \
                open("grid-vorolemap", "w") as fvorolemap, \
                open("multi-mapfile.vorole", "w") as fmvorole, \
                open("multi-mapfile.user", "w") as fmuser, \
                open("multi-mapfile.group", "w") as fmgroup, \
                open("multi-mapfile.vo", "w") as fmvo, \
                open("multi-mapfile.unmapped", "w") as fmunmapped, \
                open("vo-user.json", "w") as fvouser, \
                open("vo-group.json", "w") as fvogroup:
            fdcache.write("""# dCache global configuration
# ===========================
# location: /etc/dcache/dcache.conf
#
# If you have a big pool or many pools per domain, you probably want more
# memory in the java process than the default 512m. 1024m ought to be enough
# for a single 10TiB pool, but you probably need 2048m for a 20TiB pool. As a rule
# of thumb, use 512m + 512m for every 10 TiB of pool (rounded to a nice value).
#
# Tape write pools are usually not big and you can use 1024m. A large file system
# cache is more useful on such pools.
dcache.java.memory.heap = 4096m

# The default is 512m, but in particular with xrootd this isn't quite enough. 
#
# There is no reason to scale this by pool size - how much is required depends more on
# access patterns. For Alice tape write pools this MUST be at least 2048m.
dcache.java.memory.direct = 2048m


# dCache services configuration
dcache.layout = layout-${{host.fqdn}}


# database configuration
#dcache.db.host = localhost
#dcache.db.user = dcache
#dcache.db.password =


# Diskonly storage
# https://dcache.org/old/manuals/Book-8.2/config-SRM.shtml#utilization-of-space-reservations-for-data-storage
dcache.default-retention-policy=REPLICA
dcache.default-access-latency=ONLINE

# enable ACL support
pnfsmanager.enable.acl = true


# Explicit port configuration for LAN/WAN access with GridFTP and HTTP protocol
dcache.net.wan.port.min = 20000
dcache.net.wan.port.max = 25000
# LAN port range for internal pool to pool communication
dcache.net.lan.port.min = 33115
dcache.net.lan.port.max = 33145
# use same ports for all protocols (GridFTP, WebDAV, xroot)
pool.mover.xrootd.port.min = ${{dcache.net.wan.port.min}}
pool.mover.xrootd.port.max = ${{dcache.net.wan.port.max}}

# Allow Let's encrypt certificates that doesn't provide CRLs
#dcache.authn.hostcert.verify=true
#dcache.authn.crl-mode = IF_VALID


# BDII Glue Info Provider
# Besides these two basic configuration option it's necessary to update also
# /etc/dcache/info-provider.xml and install bdii packages for details see
# https://dcache.org/old/manuals/Book-8.2/config-info-provider.shtml
info-provider.site-unique-id=GOCDB_SITE_NAME
info-provider.se-unique-id=dcache.example.com


# EGI StAR (APEL Storage Accounting)
#star.gid-mapping = 2000=/atlas, 1010=/dteam, 1020=/wlcg


# By default, dCache auto initializes pool directories. To avoid
# accidentally initializing a pool in a mount point, we configure all
# pools to wait for the pool file system to be mounted.
pool.wait-for-files = ${{pool.path}}/data

# All pools are tagged by the FQDN of the host. dCache uses this to avoid
# replicating files to pools on the same host.
pool.tags = hostname=${{host.fqdn}}

# To be able to use xroot-tpc with EOS it is necessary to add "unix" (now enabled by default)
#pool.mover.xrootd.tpc-authn-plugins=gsi,unix
#pool.mover.xrootd.plugins=gsi,unix

# pool startup optimization
# https://indico.desy.de/event/25462/contributions/57176/attachments/36943/46186/dcache-project-whatsnew.pdf
pool.limits.scan-threads=8
pool.plugins.meta.db!je.checkpointer.wakeupInterval = 60 s
pool.plugins.meta.db!je.checkpointer.bytesInterval = 0
pool.plugins.meta.db!je.cleaner.wakeupInterval= 0 s
pool.plugins.meta.db!je.log.fileCacheSize=1024


# Enable writing with xroot protocol (be aware that xroot protocol doesn't
# protect established connection from packet content changes on the wire
# unless you enable integrity validation or TLS)
xrootd.authz.write-paths = /

# Enable xroot protocol integrity validation for sensitive operations like write.
# Not compatible with xrd client < 4.5, but recommended in case of enabled xroot
# writing over wan network. This mechanism is deprecated in favor of TLS that's
# available since xrd >= 5.0
#dcache.xrootd.security.level=4
#dcache.xrootd.security.force-signing=true

# Enable TLS for xroot protocol
# https://indico.desy.de/event/29564/contributions/104597/attachments/66122/81844/dCache-Users-xroot-config.pptx
# https://dcache.org/old/manuals/Book-8.2/config-xrootd.shtml#token-based-authorization
#pool.mover.xrootd.security.tls.mode=OPTIONAL
#pool.mover.xrootd.security.tls.require-login=true

# Third party copy transfer limits
#transfermanagers.limits.transfer-time = 7200
#transfermanagers.limits.internal-transfers = 150
#transfermanagers.limits.external-transfers = 1000

# Location of LinkGroupAuthorizationFile with VOMS FQAN allowed to reserve space from link group
spacemanager.authz.link-group-file-name=/etc/dcache/LinkGroupAuthorization.conf
""".format(self._config['headnode']))
            fgplazma.write("""# dCache PAM-style authorization configuration
# ============================================
# documentation: https://dcache.org/old/manuals/Book-8.2/config-gplazma.shtml
#                https://dcache.org/old/manuals/2014/presentations/20140324-PM-ISGC-gplazma.pdf
# location: gplazma.configuration.file=/etc/dcache/gplazma.conf
#

auth     optional     x509
auth     optional     voms
auth     optional     scitoken
#auth     optional     oidc

#map      optional     gridmap
#map      optional     vorolemap
#map      sufficient   multimap gplazma.multimap.file=/etc/dcache/multi-mapfile.vorole
map      optional     multimap gplazma.multimap.file=/etc/dcache/multi-mapfile.group
map      optional     vogroup vo-group-path=/etc/dcache/vo-group.json
map      sufficient   multimap gplazma.multimap.file=/etc/dcache/multi-mapfile.user
map      optional     vogroup vo-group-path=/etc/dcache/vo-user.json
map      sufficient   multimap gplazma.multimap.file=/etc/dcache/multi-mapfile.vo
map      sufficient   multimap gplazma.multimap.file=/etc/dcache/multi-mapfile.unmapped
#map      requisite    nsswitch
#map      sufficient   ldap

account  requisite  banfile
# before enabling ARGUS update gplazma.argus.endpoint
#account  requisite  argus

session  requisite  roles
session  sufficient omnisession
#session  requisite  nsswitch
#session  optional   ldap

#identity optional   nsswitch
#identity requisite  ldap
""")
            fomnisession.write("""# Omnisession plugin (omnisession) configuration
# ==============================================
# documentation: https://www.dcache.org/old/manuals/Book-8.2/config-gplazma.shtml#omnisession
# location: gplazma.omnisession.file=/etc/dcache/omnisession.conf
#
# This file replaces storage-authzdb used in the past by authzdb plugin.
# It is available already in 6.2.26+ and all 7.2.x, 8.2.x dCache releases.
# You should not use deprecated authzdb plugin and this simple configuration
# where "group:writer" have privileges to the whole namespace should be
# sufficient (fine grained permissions comes from user/group/ACLs).

# set home directories for local users that we support on this storage
#username:vokac       home:/users/vokac
#username:admin       home:/users/admin root:/

# default root path and home directory for supported VOs
#group:atlascz        home:/groups/atlas/cz
#username:atlas       root:/ home:/
#username:atlas_oidc  root:/ home:/

# fallback
group:writer         root:/ home:/
#group:reader         read-only root:/ home:/

#DEFAULT              read-only root:/ home:/
""")
            fbanfile.write("""# Banfile plugin (banfile) configuration
# ======================================
# documentation: https://dcache.org/old/manuals/Book-8.2/config-gplazma.shtml#banfile
# location: gplazma.banfile.path=/etc/dcache/ban.conf
# gplazma: map optional banfile
#
# User defined aliases for ban predicates
#alias dn=org.globus.gsi.gssapi.jaas.GlobusPrincipal
#alias group=org.dcache.auth.GroupNamePrincipal
#alias fqan=org.dcache.auth.FQANPrincipal
#alias kerberos=javax.security.auth.kerberos.KerberosPrincipal
#alias oidc=org.dcache.auth.OidcSubjectPrincipal
#alias oidcgrp=org.dcache.auth.OpenIdGroupPrincipal
#alias username=org.dcache.auth.UserNamePrincipal
#alias entitlement=org.dcache.auth.EntitlementPrincipal
#alias name=org.dcache.auth.LoginNamePrincipal
#
# examples how to ban clients
#ban dn:/DC=org/DC=terena/DC=tcs/C=CZ/O=Czech Technical University in Prague/CN=Petr Vokac 252509
#ban fqan:/dteam/Role=test
#ban username:vokac
""")
            fvorolemap.write("""# VOMS plugin (vorolemap) configuration
# =====================================
# documentation: https://www.dcache.org/old/manuals/Book-8.2/config-gplazma.shtml#vorolemap
# location: gplazma.vorolemap.file=/etc/grid-security/grid-vorolemap
# gplazma: map optional vorolemap
#
# This module can be used to provide specific mapping for user+vo role/group
# pair. For mapping just individial users or individual vo role/groups
# it should be sufficient to rely just on multimap plugin.
#
# This VOMS plugin is not enabled by default in gplazma.conf, it must be
# used together with multimap plugin with multi-mapfile.vorole config file

# vo+role mapping example
"/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=vokac/CN=610071/CN=Petr Vokac" "/atlas" atlas_vokac_vorole
"/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=vokac/CN=610071/CN=Petr Vokac" "/atlas/Role=pilot" atlasplt_vokac_vorole
"/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=vokac/CN=610071/CN=Petr Vokac" "/atlas/Role=production" atlasprd_vokac_vorole
"/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=vokac/CN=610071/CN=Petr Vokac" "/atlas/Role=lcgadmin" atlassgm_vokac_vorole
"/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=vokac/CN=610071/CN=Petr Vokac" "/atlas/cz" atlascz_vokac_vorole
"/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=vokac/CN=610071/CN=Petr Vokac" "/atlas/de" atlasde_vokac_vorole
"/DC=org/DC=terena/DC=tcs/C=CZ/O=Czech Technical University in Prague/CN=Petr Vokac 252509" "/atlas" atlas_vokac_vorole
"/DC=org/DC=terena/DC=tcs/C=CZ/O=Czech Technical University in Prague/CN=Petr Vokac 252509" "/atlas/Role=pilot" atlasplt_vokac_vorole
"/DC=org/DC=terena/DC=tcs/C=CZ/O=Czech Technical University in Prague/CN=Petr Vokac 252509" "/atlas/Role=production" atlasprd_vokac_vorole
"/DC=org/DC=terena/DC=tcs/C=CZ/O=Czech Technical University in Prague/CN=Petr Vokac 252509" "/atlas/Role=lcgadmin" atlassgm_vokac_vorole
"/DC=org/DC=terena/DC=tcs/C=CZ/O=Czech Technical University in Prague/CN=Petr Vokac 252509" "/atlas/cz" atlascz_vokac_vorole
"/DC=org/DC=terena/DC=tcs/C=CZ/O=Czech Technical University in Prague/CN=Petr Vokac 252509" "/atlas/de" atlasde_vokac_vorole

# this can be done also directly in multimap configuration file
#"*" "/wlcg" wlcg_vorole
#"*" "/dteam" dteam_vorole
#"*" "/atlas" atlas_vorole
#"*" "/atlas/Role=pilot" atlasplt_vorole
#"*" "/atlas/Role=production" atlasprd_vorole
#"*" "/atlas/Role=lcgadmin" atlassgm_vorole
#"*" "/atlas/cz" atlascz_vorole
#"*" "/atlas/de" atlasde_vorole
""")
            fmvorole.write("""# MultiMap plugin (multimap) configuration
# ========================================
# documentation: https://www.dcache.org/old/manuals/Book-8.2/config-gplazma.shtml#multimap-plugin-multimap
# location: gplazma.multimap.file=/etc/dcache/multi-mapfile.vorole
# gplazma: map sufficient multimap gplazma.multimap.file=/etc/dcache/multi-mapfile.vorole
#
# mapping sources:
#   group:*_vorole comes from vorolemap plugin configuration (disabled / deprecated)
#   group:*_oidc comes from gplazma.scitoken.issuer configuration
#
# This MultimMap plugin is not enabled by default in gplazma.conf, it rely on VOMS
# plugin and groups defined by grid-vorolemap config file

# DTEAM
group:dteam_vorole             uid:1000 gid:1000,true username:dteam group:writer


# WLCG
group:wlcg_vorole              uid:1100 gid:1100,true username:wlcg group:writer


# ATLAS identity mappings
group:atlas_vorole             uid:2000 gid:2000,true username:atlas group:writer
group:atlasplt_vorole          gid:2010
group:atlasprd_vorole          gid:2011
group:atlassgm_vorole          gid:2012
group:atlascz_vorole           gid:2100
group:atlasde_vorole           gid:2110

group:atlas_vokac_vorole       uid:10000 gid:2000,true username:vokac group:writer
group:atlasplt_vokac_vorole    gid:2010
group:atlasprd_vokac_vorole    gid:2011
group:atlassgm_vokac_vorole    gid:2012
group:atlascz_vokac_vorole     gid:2100
group:atlasde_vokac_vorole     gid:2110
""")
            fmuser.write("""# MultiMap plugin (multimap) configuration
# ========================================
# documentation: https://www.dcache.org/old/manuals/Book-8.2/config-gplazma.shtml#multimap-plugin-multimap
# location: gplazma.multimap.file=/etc/dcache/multi-mapfile.user
# gplazma: map sufficient multimap gplazma.multimap.file=/etc/dcache/multi-mapfile.user
#
# mapping sources:
#   group:*_vorole comes from vorolemap plugin configuration (disabled / deprecated)
#   group:*_oidc comes from gplazma.scitoken.issuer configuration
#
# example - individual mapping for local users:
#oidc:b0096aea-4eda-4d58-89bc-4c1880e78cb8@atlas uid:10000
#oidc:58280cfd-ed7f-4954-90c7-cfde610cb963@wlcg uid:10000
#"dn:/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=vokac/CN=610071/CN=Petr Vokac" username:vokac uid:10000
#"dn:/DC=org/DC=terena/DC=tcs/C=CZ/O=Czech Technical University in Prague/CN=Petr Vokac 252509" username:vokac uid:10000

# generated from imported config file
""")
            fmgroup.write("""# MultiMap plugin (multimap) configuration
# ========================================
# documentation: https://www.dcache.org/old/manuals/Book-8.2/config-gplazma.shtml#multimap-plugin-multimap
# location: gplazma.multimap.file=/etc/dcache/multi-mapfile.group
# gplazma: map optional multimap gplazma.multimap.file=/etc/dcache/multi-mapfile.group
#
# mapping sources:
#   group:*_vorole comes from vorolemap plugin configuration (disabled / deprecated)
#   group:*_oidc comes from gplazma.scitoken.issuer configuration
#
# example for DTEAM:
#fqan:/dteam                    gid:1000 group:writer
#group:oidc_dteam               gid:1001,true group:writer
#oidcgrp:/dteam                 gid:1000

# generated from imported config file
""")
            fmvo.write("""# MultiMap plugin (multimap) configuration
# ========================================
# documentation: https://www.dcache.org/old/manuals/Book-8.2/config-gplazma.shtml#multimap-plugin-multimap
# location: gplazma.multimap.file=/etc/dcache/multi-mapfile.vo
# gplazma: map sufficient multimap gplazma.multimap.file=/etc/dcache/multi-mapfile.vo
#
# mapping sources:
#   group:*_vorole comes from vorolemap plugin configuration (disabled / deprecated)
#   group:*_oidc comes from gplazma.scitoken.issuer configuration
#
# example for DTEAM:
#fqan:/dteam                    uid:1000 username:dteam
#username:dteam_oidc            uid:1001

# generated from imported config file
""")
            fmunmapped.write("""# MultiMap plugin (multimap) configuration
# ========================================
# documentation: https://www.dcache.org/old/manuals/Book-8.2/config-gplazma.shtml#multimap-plugin-multimap
# location: gplazma.multimap.file=/etc/dcache/multi-mapfile.unmapped
# gplazma: map sufficient multimap gplazma.multimap.file=/etc/dcache/multi-mapfile.unmapped
#
# mapping sources:
#   group:*_vorole comes from vorolemap plugin configuration (disabled / deprecated)
#   group:*_oidc comes from gplazma.scitoken.issuer configuration
#
# example for DTEAM mapping for groups and roles with no explicit configuration
# (remove to grant access only to explicitly configured VO FQAN):
#fqan:/dteam                   uid:1000 gid:1000,true username:dteam

# generated from imported config file
""")
            # primary group user name and uid
            group2uid = {}
            #for orig_group, gdata in sorted(self._config['group'].items()):
            #    group, gid = gdata
            #    group2uid[group] = (group, gid)
            for group, gdata in sorted(self._config['group2uid'].items()):
                user, uid = gdata
                group2uid[group] = (user, uid)

            for orig_user, udata in sorted(self._config['user'].items()):
                if not orig_user.startswith('/'): continue
                user, uid = udata
                fmuser.write("\"dn:{0:<27}\" username:{1} uid:{2}\n".format(orig_user, user, uid))

            first_fqan = []
            last_top_group = None
            for orig_group, gdata in sorted(self._config['group'].items()):
                if not orig_group.startswith('/'): continue
                group, gid = gdata
                top_group = group if group.find('_') == -1 else group[:group.find('_')]
                is_top_group = group.find('_') == -1

                if last_top_group != top_group:
                    fmgroup.write("\n")
                    fmgroup.write("# {0} mapping of client groups/roles to gid numbers\n".format(group.upper()))
                    fmvo.write("\n")
                    fmvo.write("# {0} mapping of username resolved by vogroup plugin to uid\n".format(group.upper()))
                    fmunmapped.write("\n")
                    fmunmapped.write("# {0} default mapping for groups and roles with no explicit configuration\n".format(group.upper()))
                    last_top_group = top_group

                fmgroup.write("fqan:{0:<25} gid:{1}{2}\n".format(orig_group, gid, " group:writer" if is_top_group else ""))
                if is_top_group:
                    fmgroup.write("#op:{0:<26} gid:{1},true group:writer username:{2}_oidc\n".format(group, gid, group))
                fmgroup.write("#oidcgrp:{0:<21} gid:{1}\n".format(orig_group.replace('/Role=', '/'), gid))

                user, uid = group2uid[group] if group in group2uid else (group, gid)
                fmvo.write("username:{0:<21} uid:{1}\n".format(user, uid))
                if is_top_group:
                    fmvo.write("#username:{0:<21} uid:{1}\n".format("{0}_oidc".format(user), uid))
                    fmunmapped.write("fqan:{0:<25} uid:{1} gid:{2},true username:{3}\n".format(orig_group, uid, gid, user))

                first_fqan.append({
                    'fqan': orig_group,
                    'mapped_gid': gid,
                    'mapped_uname': user,
                })

            fvouser.write(json.dumps(first_fqan, indent=2))
            fvogroup.write(json.dumps([{'fqan': x['fqan'], 'mapped_gid': x['mapped_gid']} for x in first_fqan], indent=2))

        with open("layout-{0}.conf".format(self._config['headnode']), "w") as f:
            f.write("""# Headnode dCache layout configuration
# ====================================
# location: /etc/dcache/layouts/layout-{0}.conf

[centralDomain]
dcache.broker.scheme = core
[centralDomain/zookeeper]
[centralDomain/pnfsmanager]
[centralDomain/cleaner]
[centralDomain/poolmanager]
[centralDomain/spacemanager]
[centralDomain/pinmanager]
[centralDomain/billing]
[centralDomain/gplazma]
# SciToken plugin configuration for dCache 7.2.x
# https://github.com/dCache/dcache/blob/7.2.22/skel/share/defaults/gplazma.properties#L471-L559
#gplazma.scitoken.issuer!wlcg = https://wlcg.cloud.cnaf.infn.it/ /dpm/example.com/home/wlcg
#gplazma.scitoken.issuer!altas = https://atlas-auth.web.cern.ch/ /dpm/example.com/home/atlas
#gplazma.scitoken.audience-targets = https://headnode.example.com
# OIDC plugin configuration for dCache 8.2.x
#gplazma.oidc.provider!wlcg = https://wlcg.cloud.cnaf.infn.it/ -profile=wlcg -prefix=/dpm/example.com/home/wlcg
#gplazma.oidc.provider!altas = https://atlas-auth.web.cern.ch/ -profile=wlcg -prefix=/dpm/example.com/home/atlas
#gplazma.oidc.audience-targets = https://headnode.example.com
# https://www.dcache.org/old/manuals/Book-8.2/config-gplazma.shtml#argus
#gplazma.argus.endpoint = https://argus.example.com:8154/authz


[adminDoorDomain]
[adminDoorDomain/admin]


[informationDomain]
[informationDomain/httpd]
[informationDomain/topo]
[informationDomain/info]
[informationDomain/statistics]
[informationDomain/frontend]
frontend.authn.basic=true
frontend.authn.protocol=http
frontend.authz.anonymous-operations=READONLY
frontend.srr.public=true
#frontend.srr.shares=ATLASDATADISK:/dpm/example.com/home/atlas/atlasdatadisk
#[informationDomain/telemetry]
#telemetry.cell.enable=true
#telemetry.instance.site-name=praguelcg2 dCache testbed
#telemetry.instance.location.latitude=50.1243308
#telemetry.instance.location.longitude=14.4686006


# To run WebDAV on privileged port < 1024 it is necessary to add systemd
# AmbientCapabilities=CAP_NET_BIND_SERVICE privileges to dCache service
[doorsDomain]
[doorsDomain/webdav]
webdav.authn.protocol = https
webdav.net.port = 443

[doorsDomain/xrootd]
xrootd.plugins=gplazma:gsi

[doorsDomain/ftp]
ftp.authn.protocol=gsi

# uncomment to enable NFS protocol, for details see documentation
# https://dcache.org/old/manuals/Book-8.2/config-nfs.shtml
#[doorsDomain/nfs]
#nfs.version = 4.1


[srmDomain]
[srmDomain/srm]
srm.net.port = 8446

[srmmanagerDomain]
[srmmanagerDomain/srmmanager]
[srmmanagerDomain/transfermanagers]
""".format(self._config['headnode']))

        hostcfg = {}
        hosts = set()
        for project, spacetoken, host, fs, used, expected in sorted(self._config['used']):
            prj_used, prj_expected = hostcfg.get((host, fs, project), (0, 0))
            hostcfg[(host, fs, project)] = (prj_used + used, prj_expected + expected)
            hosts.add(host)
        for host in sorted(hosts):
            headdisk = host == self._config['headnode']
            with open("layout-{0}.conf".format(host), "a" if headdisk else "w") as f:
                f.write("# Disknode dCache layout configuration\n")
                f.write("# ====================================\n")
                f.write("# location: /etc/dcache/layouts/layout-{0}.conf\n".format(host))
                f.write("\n")
                f.write("# disknodes needs to know how to contact central node\n")
                if headdisk: f.write("#")
                f.write("dcache.zookeeper.connection = {0}:2181\n".format(self._config['headnode']))
                f.write("\n")
                fs2info = {}
                for filesystem, fdata in sorted(self._config['filesystem'].items()):
                    fs_host, fs_fs = filesystem
                    if host != fs_host: continue
                    projects, spacetokens, space, used = fdata
                    fs2info[fs_fs] = (spacetokens, space)
                for fs in sorted(fs2info.keys()):
                    spacetokens, space = fs2info[fs]
                    if space != 0:
                        f.write("# filesystem {0} size {1} (spacetokens: {2})\n".format(fs, prettySize(space), ', '.join(spacetokens)))
                    else:
                        f.write("# filesystem {0} (spacetokens: {1})\n".format(fs, ', '.join(spacetokens)))
                f.write("\n")
                if not headdisk: f.write("""
[doorsDomain-${host.name}]
[doorsDomain-${host.name}/webdav]
webdav.authn.basic = true
webdav.authn.protocol = https
webdav.loginbroker.port = 443

[doorsDomain-${host.name}/xrootd]
xrootd.plugins=gplazma:gsi

[doorsDomain-${host.name}/ftp]
ftp.authn.protocol=gsi

""")
                for hostcfg_host, fs, project in hostcfg.keys():
                    if hostcfg_host != host: continue
                    used, expected = hostcfg[(host, fs, project)]
                    #fs_projects, fs_spacetokens, space, _ = self._config['filesystem'][(host, fs)]
                    dpool = self._get_dpool(host, fs, project)
                    f.write("# project {0} on filesystem {1}\n".format(project, fs))
                    f.write("[poolsDomain_${{host.name}}_{1}]\n".format(host.replace('.', '_'), project))
                    f.write("[poolsDomain_${{host.name}}_{1}/pool]\n".format(host.replace('.', '_'), project))
                    f.write("pool.name={0}\n".format(dpool))
                    f.write("pool.tags=hostname=${{host.name}} poolgroup={0}\n".format(project))
                    f.write("pool.path={0}/dcache/{1}\n".format(fs, dpool))
                    # pool.size is zero when DPM DB dump was not done on headnode
                    #f.write("pool.size={0}G\n".format(expected / 1024**3))
                    #f.write("pool.wait-for-files=${pool.path}/data\n")
                    f.write("\n")

        with open("admin-cli.psu", "w") as f:
            f.write("# Configure Poole manager for space reservation\n")
            f.write("# Use dCache admin shell to import this configuration file\n")
            f.write("#   cat admin-cli.psu | grep -v ^# | ssh -p 22224 -l admin localhost\n")
            f.write("# To see configuration of pool manager call\n")
            f.write("#   ssh -p 22224 -l admin localhost '\c PoolManager; psu dump setup'\n")
            f.write("# or look in the configuration file /var/lib/dcache/config/poolmanager.conf\n")
            f.write("\n")
            f.write("\\c PoolManager\n")
            f.write("\n")
            dpool_removed = set()
            for project, spacetoken, host, fs, used, expected in self._config['used']:
                dpool = self._get_dpool(host, fs, project)
                if dpool in dpool_removed: continue
                f.write("#psu removefrom pgroup default {0}\n".format(dpool))
                dpool_removed.add(dpool)
            f.write("psu remove pgroup default\n")
            f.write("\n")
            for project, pdata in sorted(self._config['project'].items()):
                f.write("psu create pgroup -dynamic -tags=poolgroup={0} spacemanager_poolGroup_{0}\n".format(project))
                f.write("psu create link spacemanager_link_{0} any-store world-net any-protocol\n".format(project))
                f.write("psu set link spacemanager_link_{0} -readpref=10 -writepref=10 -cachepref=0 -p2ppref=-1\n".format(project))
                f.write("psu addto link spacemanager_link_{0} spacemanager_poolGroup_{0}\n".format(project))
                f.write("psu create linkGroup spacemanager_linkGroup_{0}\n".format(project))
                f.write("psu set linkGroup replicaAllowed spacemanager_linkGroup_{0} true\n".format(project))
                f.write("psu set linkGroup onlineAllowed spacemanager_linkGroup_{0} true\n".format(project))
                f.write("psu addto linkGroup spacemanager_linkGroup_{0} spacemanager_link_{0}\n".format(project))
                f.write("\n")
            f.write("save\n")
        with open("admin-cli.reserve", "w") as f:
            f.write("# Reserve (empty) space to for migrated quotatokens\n")
            f.write("# Use dCache admin shell to import this configuration file\n")
            f.write("#   cat admin-cli.reserve | grep -v ^# | ssh -p 22224 -l admin localhost\n")
            f.write("# To see configuration of space manager call\n")
            f.write("#   ssh -p 22224 -l admin localhost '\c SrmSpaceManager; ls link groups'\n")
            f.write("#   ssh -p 22224 -l admin localhost '\c SrmSpaceManager; ls spaces -e'\n")
            f.write("#\n")
            f.write("# This script assign 0 size to all new spacetokens, because it is executed\n")
            f.write("# before we start dCache services on disknodes and that means we don't yet\n")
            f.write("# have any space available for reservation. Correct size will be automatically\n")
            f.write("# set during dCache namespace import and you can always change spacetoken space\n")
            f.write("# reservation later using: update space -size=SIZE TOKEN_ID\n")
            f.write("#\n")
            f.write("# Migrated spacetokens and their original size:\n")
            for spacetoken, sdata in sorted(self._config['spacetoken'].items()):
                project, path, groups, space, used = sdata
                f.write("#   {0} {1} ({2})\n".format(spacetoken, space, prettySize(space)))
            f.write("\n")
            f.write("\\c SrmSpaceManager\n")
            f.write("\n")
            for spacetoken, sdata in sorted(self._config['spacetoken'].items()):
                project, path, groups, space, used = sdata
                f.write("reserve space -desc=\"{0}\" -al=online -rp=replica -lg=spacemanager_linkGroup_{1} 0\n".format(spacetoken, project))

        with open("LinkGroupAuthorization.conf", "w") as f:
            f.write("# SpaceManagerLinkGroupAuthorizationFile\n")
            f.write("# ======================================\n")
            f.write("# location: spacemanager.authz.link-group-file-name=/etc/dcache/LinkGroupAuthorization.conf\n")
            f.write("#\n")
            f.write("# example\n")
            f.write("#LinkGroup spacemanager_linkGroup\n")
            f.write("#*/Role=*\n")
            prj2groups = {}
            for spacetoken, sdata in sorted(self._config['spacetoken'].items()):
                project, path, groups, space, used = sdata
                for group in groups:
                    prj2groups.setdefault(project, set()).add(group)
            for project, groups in sorted(prj2groups.items()):
                f.write("\n")
                f.write("spacemanager_linkGroup_{0}\n".format(project))
                for group in groups:
                    if group.find('/Role=') == -1:
                        f.write("{0}/Role=*\n".format(group))
                    else:
                        f.write("{0}\n".format(group))

        with open("writetoken.chimera", "w") as f:
            f.write("# Update WriteTokens to match spacetoken path\n")
            f.write("# NOTE: this might be useful only if you imported dCache\n")
            f.write("# namespace with '--skip-dcache-writetoken' commandline option\n")
            f.write("# NOTE: this script doesn't move data files between spacetokens\n")
            f.write("# and space used by file in the directories labeled by new WriteToken\n")
            f.write("# is not going to be associated with chosen spacetoken (dCache doesn't\n")
            f.write("# provide tools to move data between spacetokens)\n")
            f.write("#\n")
            f.write("# List existing spacetokens with tokenid and description\n")
            f.write("#   ssh -p 22224 -l admin localhost '\c SrmSpaceManager; ls spaces'\n")
            f.write("# (first column is tokenid, last column is spacetoken description)\n")
            for spacetoken in sorted(self._config['spacetoken'].keys()):
                project, path, groups, space, used = self._config['spacetoken'][spacetoken]
                f.write("chimera writetag {0} replace_with_real_tokenid_for_{1}\n".format(path, spacetoken))
                f.write("chimera pushtag {0}\n".format(path))


    def use_writetoken_for_spacetokens(self, updatedb=False):
        _log.debug("use WriteToken tags for all files (updatedb=%s)", updatedb)

        inserted = 0
        updated = 0
        deleted = 0

        cursor = self._conn.cursor()
        cursor_spacemanager = self._conn_spacemanager.cursor()

        # add/update file spacetoken according WriteToken parent directory tag
        sql = """
SELECT ipnfsid, t_inodes.isize, nullif(convert_from(btrim(t_tags_inodes.ivalue, '\x0a'), 'UTF8'), '')::integer
FROM t_inodes
  JOIN t_dirs ON t_dirs.ichild = t_inodes.inumber
  JOIN t_tags ON t_dirs.iparent = t_tags.inumber
  JOIN t_tags_inodes ON t_tags.itagid = t_tags_inodes.itagid
WHERE itype & 32768 = 32768 AND t_tags.itagname = 'WriteToken'
"""
        cursor.execute(sql)
        while True:
            rows = cursor.fetchmany(10000)
            if len(rows) == 0: break
            pnfsid2size = {}
            pnfsid2reservationid = {}
            for pnfsid, size, reservationid in rows:
                pnfsid2size[pnfsid] = size
                pnfsid2reservationid[pnfsid] = reservationid
            sql = "SELECT pnfsid, spacereservationid FROM srmspacefile WHERE pnfsid IN ('{0}')".format("','".join(pnfsid2reservationid.keys()))
            cursor_spacemanager.execute(sql)
            pnfsid2reservationid_existing = dict([(pnfsid, reservationid) for pnfsid, reservationid in cursor_spacemanager.fetchall()])
            for pnfsid, reservationid in pnfsid2reservationid.items():
                if pnfsid not in pnfsid2reservationid_existing:
                    _log.log(logging.DEBUG-1 if updatedb else logging.DEBUG, "associate pnfsid %s with new spacetoken tokenid %s (size=%i)", pnfsid, reservationid, pnfsid2size[pnfsid])
                    sql = "INSERT INTO srmspacefile (vogroup, vorole, spacereservationid, sizeinbytes, creationtime, pnfsid, state) VALUES (%s,%s,%s,%s,(EXTRACT(EPOCH FROM NOW()) * 1000)::bigint,%s,%s)"
                    if updatedb:
                        cursor_spacemanager.execute(sql, (None, None, reservationid, pnfsid2size[pnfsid], pnfsid, dcache.SpaceState_EXPIRED))
                    inserted += 1
                    if inserted % 10000 == 0:
                        _log.debug("%i files associated with new spacetoken", inserted)
                else:
                    if reservationid != pnfsid2reservationid_existing[pnfsid]:
                        _log.log(logging.DEBUG-1 if updatedb else logging.DEBUG, "update pnfsid %s spacetoken tokenid %s -> %s", pnfsid, pnfsid2reservationid_existing[pnfsid], reservationid)
                        sql = "UPDATE srmspacefile SET spacereservationid = %s WHERE pnfsid = %s"
                        if updatedb:
                            cursor_spacemanager.execute(sql, (reservationid, pnfsid))
                        updated += 1
                        if updated % 10000 == 0:
                            _log.debug("%i files associated to different spacetoken", updated)
                if updatedb and (inserted + updated) % self._commit_size == 0:
                    self._conn_spacemanager.commit()
        if updatedb:
            self._conn_spacemanager.commit()

        # remove files with pnfsid not in chimera namespace
        sql = "SELECT pnfsid FROM srmspacefile"
        cursor_spacemanager.execute(sql)
        while True:
            rows = cursor_spacemanager.fetchmany(10000)
            if len(rows) == 0: break
            pnfsids = set([row[0] for row in rows])
            sql = "SELECT ipnfsid, isize FROM t_inodes WHERE ipnfsid IN ('{0}')".format("','".join(pnfsids))
            cursor.execute(sql)
            pnfsids_existing = set([row[0] for row in cursor.fetchall()])
            pnfsids_delete = pnfsids.difference(pnfsids_existing)
            if len(pnfsids_delete) == 0: continue
            for pnfsid in pnfsids_delete:
                _log.log(logging.DEBUG-1 if updatedb else logging.DEBUG, "remove spacetoken for pnfsid %s", pnfsid)
            sql = "DELETE FROM srmspacefile WHERE pnfsid IN ('{0}')".format("','".join(pnfsids_delete))
            if updatedb:
                cursor_delete = self._conn_spacemanager.cursor()
                cursor_delete.execute(sql)
                cursor_delete.close()
                self._conn_spacemanager.commit()
            if deleted // 10000 != (deleted + len(pnfsids_delete)) // 10000:
                _log.debug("%i files removed from spacetoken", deleted + len(pnfsids_delete))
            deleted += len(pnfsids_delete)

        _log.debug("file spacetoken corrections: inserted %i, updated %i, deleted %i", inserted, updated, deleted)



def prettySize(size):
    isize = int(size) # argument can be string
    if isize < 1024**1:
        prettySize = "%iB" % isize
    elif isize < 1024**2:
        prettySize = '%.2fkB' % (float(isize) / 1024**1)
    elif isize < 1024**3:
        prettySize = '%.2fMB' % (float(isize) / 1024**2)
    elif isize < 1024**4:
        prettySize = '%.2fGB' % (float(isize) / 1024**3)
    elif isize < 1024**5:
        prettySize = '%.2fTB' % (float(isize) / 1024**4)
    else:
        prettySize = '%.2fPB' % (float(isize) / 1024**5)
    return prettySize



def _find_uid_gid(user, group):
    # find uid and gid
    uid = gid = None
    if user != None:
        try:
            u = pwd.getpwnam(user)
            uid = u.pw_uid
            gid = u.pw_gid
        except KeyError:
            raise Exception("user '{0}' doesn't exist".format(user))

    if group != None:
        try:
            g = grp.getgrnam(user)
            gid = g.gr_gid
        except KeyError:
            _log.error()
            raise Exception("group '{0}' doesn't exist".format(group))

    return (uid, gid)


def _make_dirs(base, dir_names, uid, gid):
    _log.info("verify and create destination directories %s", os.path.join(base, os.path.join(*dir_names)))

    dst_dir_name = base
    for name in dir_names:
        new_dir = False
        dst_dir_name = os.path.join(dst_dir_name, name)
        if not os.path.exists(dst_dir_name):
            _log.info("create missing directory %s", dst_dir_name)
            os.mkdir(dst_dir_name)
            new_dir = True

        dst_dir_stat = os.stat(dst_dir_name)
        if not stat.S_ISDIR(dst_dir_stat.st_mode):
            _log.error("destination path %s is not a directory", dst_dir_name)
            return False

        if uid != None:
            if dst_dir_stat.st_uid != uid or dst_dir_stat.st_gid != gid:
                _log.log(logging.DEBUG if new_dir else logging.INFO, "fixing wrong directory owner (%i -> %i) or group (%i -> %i)", dst_dir_stat.st_uid, uid, dst_dir_stat.st_gid, gid)
                os.chown(dst_dir_name, uid, gid)

    return True


def _prepare_src_dst_from_csv(filename, uid=None, gid=None, reverse=False):
    _log.info("move files in between directories according (base, relative_src, relative_dst) data in %s", filename)

    with io.open(filename, newline='') as csvfile:
        reader = csv.reader(csvfile, quotechar="'", quoting=csv.QUOTE_MINIMAL)

        base_list = set()
        base_skip = set()
        dst_dir_list = set()
        dst_dir_skip = set()
        for base, rel_src, rel_dst in reader:
            if base in base_skip:
                continue
            if base not in base_list:
                base_list.add(base)
                if not os.path.exists(base):
                    _log.error("base directory %s with data files doesn't exist", base)
                    base_skip.add(base)
                    continue

            if reverse:
                tmp = rel_src
                rel_src = rel_dst
                rel_dst = tmp

            if rel_dst == '':
                continue

            src = os.path.join(base, rel_src)
            dst = os.path.join(base, rel_dst)

            if rel_src == '':
                # just create destination directories
                dst_dir_list.add(dst)
                if not _make_dirs(base, rel_dst.split('/'), uid, gid):
                    dst_dir_skip.add(dst)
                continue

            try:
                src_stat = os.stat(src)
                if stat.S_ISDIR(src_stat.st_mode):
                    _log.error("source '%s' is directory", src)
                    continue
            except OSError as e: #FileNotFoundError available only in python3
                if e.errno != errno.ENOENT:
                    raise
                _log.warn("missing source file %s", src)
                continue

            try:
                dst_stat = os.stat(dst)
                if stat.S_ISDIR(dst_stat.st_mode):
                    _log.error("destination '%s' is directory", dst)
                if src_stat.st_ino != dst_stat.st_ino:
                    _log.error("destination file '%s' exists but doesn't match source file '%s' (inode %i != %i)", src, dst, src_stat.st_ino, dst_stat.st_ino)
                continue
            except OSError as e: #FileNotFoundError available only in python3
                if e.errno != errno.ENOENT:
                    raise

            dst_dir = os.path.dirname(dst)
            if dst_dir in dst_dir_skip:
                continue

            if dst_dir not in dst_dir_list:
                dst_dir_list.add(dst_dir)

                if not _make_dirs(base, rel_dst.split('/')[:-1], uid, gid):
                    dst_dir_skip.add(dst_dir)

                if dst_dir in dst_dir_skip:
                    continue

            yield (src, dst)

            nuid = uid
            ngid = gid
            if (nuid != None and ngid == None) or (nuid == None and ngid != None):
                st = os.stat(dst)
                if nuid == None:
                    nuid = st.st_uid
                if ngid == None:
                    ngid = st.st_gid
            if nuid != None and ngid != None:
                os.chown(dst, nuid, ngid)


def move_csv(filename, user=None, group=None, reverse=False):
    _log.info("move files in between directories according (base, relative_%s, relative_%s) data in %s", "dst" if reverse else "src", "src" if reverse else "dst", filename)

    try:
        uid, gid = _find_uid_gid(user, group)
    except Exception as e:
        _log.error(str(e))
        return

    for src, dst in _prepare_src_dst_from_csv(filename, uid, gid, reverse):
        _log.debug("rename %s to %s", src, dst)
        os.rename(src, dst)


def link_csv(filename, user=None, group=None, reverse=False):
    _log.info("link files between directories according (base, relative_%s, relative_%s) data in %s", "dst" if reverse else "src", "src" if reverse else "dst", filename)

    try:
        uid, gid = _find_uid_gid(user, group)
    except Exception as e:
        _log.error(str(e))
        return

    for src, dst in _prepare_src_dst_from_csv(filename, uid, gid, reverse):
        _log.debug("link %s to %s", src, dst)
        os.link(src, dst)


def remove_csv(filename, reverse=False):
    _log.info("remove %s files according (base, relative_%s, relative_%s) data in %s", "dst" if reverse else "src", "dst" if reverse else "src", "src" if reverse else "dst", filename)

    for src, dst in _prepare_src_dst_from_csv(filename, None, None, reverse):
        _log.debug("link %s to %s", src, dst)
        os.unlink(src)




#=====================================================================
# main - legacy interface
#=====================================================================
def main(argv):
    import hashlib
    import optparse
    import inspect

    # basic logging configuration
    streamHandler = logging.StreamHandler(sys.stderr)
    streamHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
    _log.addHandler(streamHandler)
    _log.setLevel(logging.WARN)

    # parse options from command line
    def opt_set_loglevel(option, opt, value, parser):
        loglevel = option.default
        if value != None:
            loglevel = int({
                'CRITICAL': logging.CRITICAL,
                'DEBUG': logging.DEBUG,
                'ERROR': logging.ERROR,
                'FATAL': logging.FATAL,
                'INFO': logging.INFO,
                'NOTSET': logging.NOTSET,
                'WARN': logging.WARN,
                'WARNING': logging.WARNING,
            }.get(value, value))

        _log.setLevel(loglevel)
        setattr(parser.values, option.dest, loglevel)

    # command line arguments
    usage = "usage: %prog [options]"
    description = "Create files with DPM namespace and config dumps usable for migration to different storage implementation."
    parser = optparse.OptionParser(usage=usage, description=description, version="%prog")
    parser.add_option("-v", "--verbose", dest="loglevel", action="callback", callback=opt_set_loglevel, default=logging.DEBUG, help="set log level to DEBUG")
    parser.add_option("-q", "--quiet", dest="loglevel", action="callback", callback=opt_set_loglevel, default=logging.ERROR, help="set log level to ERROR")
    parser.add_option("--log-level", dest="loglevel", action="callback", callback=opt_set_loglevel, type="string", help="set log level, default: %default")
    parser.add_option("--log-file", dest="logfile", metavar="FILE", help="set log file (default: %default)")
    parser.add_option("--log-size", dest="logsize", type="int", default=10*1024*1024, help="maximum size of log file, default: %default")
    parser.add_option("--log-backup", dest="logbackup", type="int", default=4, help="number of log backup files, default: %default")
    # db command line options
    parser.add_option('--dpm-dbhost', dest='dpm_dbhost', default='localhost', help="DPM database host, default: %default")
    parser.add_option('--dpm-dbport', dest='dpm_dbport', type="int", default=3306, help="DPM database port, default: %default")
    parser.add_option('--dpm-dbuser', dest='dpm_dbuser', default='dpm', help="DPM database user, default: %default")
    parser.add_option('--dpm-dbpasswd', dest='dpm_dbpasswd', default=None, help="DPM database password, default: %default")
    parser.add_option('--dpm-dbcns', dest='dpm_dbcns', default='cns_db', help="DPM cns database name, default: %default")
    parser.add_option('--dpm-dbdpm', dest='dpm_dbdpm', default='dpm_db', help="DPM dpm database name, default: %default")
    parser.add_option('--dcache-dbhost', dest='dcache_dbhost', default='localhost', help="dCache database host, default: %default")
    parser.add_option('--dcache-dbport', dest='dcache_dbport', type="int", default=5432, help="dCache database port, default: %default")
    parser.add_option('--dcache-dbuser', dest='dcache_dbuser', default='dcache', help="dCache database user, default: %default")
    parser.add_option('--dcache-dbpasswd', dest='dcache_dbpasswd', default=None, help="dCache database password, default: %default")
    parser.add_option('--dcache-dbname-chimera', dest='dcache_dbname_chimera', default='chimera', help="dCache chimera database name, default: %default")
    parser.add_option('--dcache-dbname-spacemanager', dest='dcache_dbname_spacemanager', default='spacemanager', help="dCache database spacemanager name, default: %default")
    # output options
    parser.add_option('--dpm-export', dest='dpm_export', default=False, action="store_true", help="Export DPM namespace and config, default: %default")
    parser.add_option('--dcache-config', dest='dcache_config', default=False, action="store_true", help="Use config to create dCache config files, default: %default")
    parser.add_option('--dcache-import', dest='dcache_import', default=False, action="store_true", help="Import dCache namespace and config, default: %default")
    parser.add_option('--dcache-first-uid', dest='dcache_first_uid', type="int", default=-1, help="First dCache uid used for mapped users (-1 ... auto), default: %default")
    parser.add_option('--dcache-first-gid', dest='dcache_first_gid', type="int", default=1000, help="First dCache gid used for mapped groups, default: %default")
    parser.add_option('--dcache-vo-gid-inc', dest='dcache_vo_gid_inc', type="int", default=1000, help="Increase automatically generated gid for different VOs, default: %default")
    parser.add_option('--namespace', dest='namespace', default='namespace.csv', help="Output file with namespace data, default: %default")
    parser.add_option('--config', dest='config', default='config.csv', help="Output file with config data, default: %default")
    parser.add_option('--skip-acl', dest='skip_acl', default=False, action="store_true", help="Ignore export/import ACL, default: %default")
    parser.add_option('--skip-dcache-writetoken', dest='skip_dcache_writetoken', default=False, action="store_true", help="Don't label directories with WriteToken, default: %default")
    #parser.add_option('--unused', dest='unused', default=False, action="store_true", help="Include unused users/groups in the output, default: %default")
    parser.add_option('--move', dest='move', default=False, action="store_true", help="Move original data files in dCache data directory, default: %default")
    parser.add_option('--move-file', dest='move_file', help="Filename with base, source and destination path in CSV format, default: %default")
    parser.add_option('--move-user', dest='move_user', default='dcache', help="Change username for moved files, default: %default")
    parser.add_option('--move-group', dest='move_group', default='dcache', help="Change groupname for moved files, default: %default")
    parser.add_option('--move-reverse', dest='move_reverse', default=False, action="store_true", help="Switch source and destination, default: %default")
    parser.add_option('--link', dest='link', default=False, action="store_true", help="Link original data files in dCache data directory, default: %default")
    parser.add_option('--link-file', dest='link_file', help="Filename with base, source and destination path in CSV format, default: %default")
    parser.add_option('--link-user', dest='link_user', default='dcache', help="Change username for linked files, default: %default")
    parser.add_option('--link-group', dest='link_group', default='dcache', help="Change groupname for linked files, default: %default")
    parser.add_option('--link-reverse', dest='link_reverse', default=False, action="store_true", help="Switch source and destination, default: %default")
    parser.add_option('--remove', dest='remove', default=False, action="store_true", help="Remove original data files, default: %default")
    parser.add_option('--remove-file', dest='remove_file', help="Filename with base, source and destination path in CSV format, default: %default")
    parser.add_option('--remove-reverse', dest='remove_reverse', default=False, action="store_true", help="Switch source and destination, default: %default")
    parser.add_option('--dcache-fix-spacetokens', dest='dcache_fix_spacetokens', default=False, action="store_true", help="Use WriteToken to fix dCache spacetokens, default: %default")
    parser.add_option('--dcache-fix-spacetokens-dry-run', dest='dcache_fix_spacetokens_dry_run', default=False, action="store_true", help="Use WriteToken to fix dCache spacetokens (dry-run only, no updates), default: %default")

    (options, args) = parser.parse_args(argv[1:])

    if options.logfile == '-':
        _log.removeHandler(streamHandler)
        streamHandler = logging.StreamHandler(sys.stdout)
        streamHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
        _log.addHandler(streamHandler)
    elif options.logfile != None and options.logfile != '':
        #fileHandler = logging.handlers.TimedRotatingFileHandler(options.logfile, 'midnight', 1, 4)
        fileHandler = logging.handlers.RotatingFileHandler(options.logfile, maxBytes=options.logsize, backupCount=options.logbackup)
        fileHandler.setFormatter(logging.Formatter("%(asctime)s [%(levelname)s](%(module)s:%(lineno)d) %(message)s", "%d %b %H:%M:%S"))
        _log.addHandler(fileHandler)
        _log.removeHandler(streamHandler)

    script_name = os.path.abspath(inspect.getfile(inspect.currentframe()))
    _log.info("command: %s", " ".join(argv))
    _log.info("script: %s", script_name)
    _log.info("version: %s", __version__)
    _log.info("sha256: %s", hashlib.sha256(open(script_name, "rb").read()).hexdigest())
    _log.info("python: %s", str(sys.version_info))

    # validate command line options

    try:
        if options.dpm_export:
            # compatibility with MySQL modules available on SLC6, CentOS7, CentOS8
            try:
                import pymysql
                import pymysql.cursors as pymysql_cursors
            except ImportError:
                try:
                    import MySQLdb as pymysql
                    import MySQLdb.cursors as pymysql_cursors
                    # implement functions missing in MySQLdb
                    if not hasattr(pymysql_cursors.BaseCursor, '__enter__'):
                        def pymysql_cursors__enter__(self):
                            return self
                        pymysql_cursors.BaseCursor.__enter__ = pymysql_cursors__enter__
                    if not hasattr(pymysql_cursors.BaseCursor, '__exit__'):
                        def pymysql_cursors__exit__(self, *exc_info):
                            del exc_info
                            self.close()
                        pymysql_cursors.BaseCursor.__exit__ = pymysql_cursors__exit__
                except ImportError:
                    sys.exit("Could not import MySQL module, please install the MySQL Python module (python2-mysql/python3-mysql or MySQL-python).")

            dpm_conn_cns = pymysql.connect(host=options.dpm_dbhost, port=options.dpm_dbport, user=options.dpm_dbuser, passwd=options.dpm_dbpasswd, db=options.dpm_dbcns)
            dpm_conn_dpm = pymysql.connect(host=options.dpm_dbhost, port=options.dpm_dbport, user=options.dpm_dbuser, passwd=options.dpm_dbpasswd, db=options.dpm_dbdpm)

            # workaround for DPM database encoding
            c = dpm_conn_cns.cursor()
            c.execute("SET NAMES 'latin1'")
            c.close()

            m = dpm(dpm_conn_cns, dpm_conn_dpm)
            m.export_csv(options.namespace, options.config, options.skip_acl)

            dpm_conn_dpm.close()
            dpm_conn_cns.close()

        dcache_config = {
            'uid-first': options.dcache_first_uid,
            'gid-first': options.dcache_first_gid,
            'vo-gid-inc': options.dcache_vo_gid_inc,
        }

        if options.dcache_config:
            m = dcache()
            m.reset_config(**dcache_config)
            m.read_config(options.config)
            m.write_config("{0}.default".format(options.config))
            m.generate_config()

        if options.dcache_import or options.dcache_fix_spacetokens or options.dcache_fix_spacetokens_dry_run:
            try:
                import psycopg2
            except ImportError:
                _log.error("missing PostgreSQL Python module psycopg2")
                raise Exception("Could not import psycopg2 module, please install the PostgreSQL Python module (python36-psycopg2/python3-psycopg2).")

            cdata = {
                'host': options.dcache_dbhost,
                'port': options.dcache_dbport,
                'user': options.dcache_dbuser,
                'password': options.dcache_dbpasswd,
            }
            cdata_chimera = copy.deepcopy(cdata)
            cdata_chimera['database'] = options.dcache_dbname_chimera
            cdata_spacemanager = copy.deepcopy(cdata)
            cdata_spacemanager['database'] = options.dcache_dbname_spacemanager

            dcache_conn_chimera = psycopg2.connect(**cdata_chimera)
            dcache_conn_spacemanager = psycopg2.connect(**cdata_spacemanager)

            m = dcache(dcache_conn_chimera, dcache_conn_spacemanager)

            if options.dcache_import:
                m.reset_config(**dcache_config)
                m.read_config(options.config)
                m.write_config("{0}.dcache-before-import".format(options.config))
                m.import_csv(options.namespace, options.skip_acl, options.skip_dcache_writetoken)
                m.write_config("{0}.dcache-after-import".format(options.config))
                #m.generate_config()

            if options.dcache_fix_spacetokens or options.dcache_fix_spacetokens_dry_run:
                m.use_writetoken_for_spacetokens(not options.dcache_fix_spacetokens_dry_run)

            dcache_conn_chimera.close()
            dcache_conn_spacemanager.close()

        if options.move:
            move_csv(options.move_file, user=options.move_user, group=options.move_group, reverse=options.move_reverse)

        if options.link:
            link_csv(options.link_file, user=options.link_user, group=options.link_group, reverse=options.link_reverse)

        if options.remove:
            remove_csv(options.remove_file, reverse=options.remove_reverse)

    except Exception as e:
        _log.error("migrate unexpected failure: %s", str(e))
        if _log.getEffectiveLevel() <= logging.DEBUG:
            import traceback
            _log.debug(traceback.format_exc())
        return 1
    finally:
        pass

    _log.info("done")

    return os.EX_OK


if __name__ == '__main__':
    sys.exit(main(sys.argv))
